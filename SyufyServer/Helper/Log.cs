﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Net.Mail;

namespace SyufyServer.Helper
{
    public class Log
    {
        private static readonly log4net.ILog log = 
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static void WriteToLog(string title, string message)
        {
            try
            {

                log.Info(string.Format("{0} {1}", title, message));

            }
            catch (Exception)
            {
                // How should we handle errors here?
            }
        }

        public static void SendEmail(string message)
        {
            try
            {
                MailMessage mail = new MailMessage();

                mail.To.Add("customerservice@somidata.com");
                mail.From = new MailAddress("customerservice@somidata.com", "Customer Service");
                mail.Subject = "Syufy Web API Error";

                string body = "An error occurred in processing a Web API request.  ";
                body += "Please take a look at the log files in the Log directory of the application and the Logs table of the application's database.";
                body += "The following is a description of the error.";

                string htmlBody = String.Format("{0} {1} {2} {3}", body, Environment.NewLine, Environment.NewLine, message);
             
                mail.Body = htmlBody;

                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "pod51019.outlook.com"; //Or Your SMTP Server Address
                smtp.Credentials = new System.Net.NetworkCredential("customerservice@somidata.com", "Toyu7413");
                //Or your Smtp Email ID and Password
                smtp.EnableSsl = true;
                smtp.Port = 587;

                smtp.Send(mail);
            }
            catch (Exception e)
            {
                Log.WriteToLog("EXCEPTION", e.ToString());
            }
        }
    }
}