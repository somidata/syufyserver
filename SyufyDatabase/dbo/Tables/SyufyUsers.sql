﻿CREATE TABLE [dbo].[SyufyUsers] (
    [Id]          INT          IDENTITY (1, 1) NOT NULL,
    [Email]       VARCHAR (50) NULL,
    [CreatedDate] DATETIME     NULL,
    CONSTRAINT [PK_SyufyUsers] PRIMARY KEY CLUSTERED ([Id] ASC)
);

