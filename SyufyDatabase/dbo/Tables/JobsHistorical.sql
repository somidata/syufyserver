﻿CREATE TABLE [dbo].[JobsHistorical] (
    [Id]                    INT             IDENTITY (1, 1) NOT NULL,
    [EmployeeId]            INT             NULL,
    [JobId]                 INT             NULL,
    [Name]                  NVARCHAR (50)   NULL,
    [OverrideRegPayRate]    DECIMAL (14, 4) NULL,
    [RateEffectiveDateTime] DATETIME2 (7)   NULL,
    [LastUpdatedDate]       DATETIME2 (7)   NULL,
    [PrimaryJob]            BIT             NULL,
    [JobSequence]           INT             NULL,
    [DefaultTemplateId]     INT             NULL,
    CONSTRAINT [PK_JobsHistorical] PRIMARY KEY CLUSTERED ([Id] ASC)
);

