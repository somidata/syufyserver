﻿
-- =============================================
-- Author:		SomiData
-- Create date: 4/8/2016
-- Description:	Updates Micros employee job data.
-- =============================================
CREATE PROCEDURE [dbo].[spUpdateEmployeeJobs]
	@LocationName varchar(100) = NULL, @LocationNumber varchar(50) = NULL, 
	@LinkedServer  varchar(100) = NULL, 
	@IncrementalUpdate int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @MyStr varchar(Max), @MyStr2 varchar(Max), @MyStr3 varchar(Max), @MyStrAll varchar(Max),
			@LocationId varchar(100), @EmployeeId varchar(100), @FirstName varchar(100), @LastName varchar(100), 
			@ObjectNumber varchar(100), @LongFirstName varchar(100), @LongLastName varchar(100), @MiddleName varchar(100),
			@CheckName varchar(100), @CheckName2 varchar(100), @EffectiveFrom varchar(100), @DateOfBirth varchar(100),
			@PartTime varchar(100), @Salaried varchar(100), @AddressLine1 varchar(100), @AddressLine2 varchar(100),
			@City varchar(100), @PostalCode varchar(100), @LocalNumber varchar(100), @OtherNumber varchar(100), 
			@Email varchar(100), @EmergencyName varchar(100), @EmergencyNumber varchar(100), @ContactRelation varchar(100),
			@Gender varchar(100), @HireDate varchar(100), @OriginalHireDate varchar(100), @SalaryStartDate varchar(100), 
			@HourlySalary varchar(100), @AnnualSalary varchar(100), @ClassSequence varchar(100),
			@BoClassSequence varchar(100), @LastUpdatedDate varchar(100), @RevenueCenterSeq varchar(100),
			@TerminationDate varchar(100), @TerminationNote varchar(100), @Status varchar(100), 
			@UpdateRecord varchar(100), @IsTerminated varchar(100), @JobSequence varchar(100), @JobId varchar(100), 
			@Name varchar(100), @OverrideRegPayRate varchar(100), @RateEffectiveDateTime varchar(100), 
			@PrimaryJob varchar(100), @EmployeeSequence varchar(100)
			
	
	DECLARE CurEmployeeUpdate CURSOR FOR
	SELECT
		 [LocationId],[EmployeeId],[FirstName],[LastName],[ObjectNumber],[LongFirstName],[LongLastName],[MiddleName]
		,[CheckName],[CheckName2],[EffectiveFrom],[DateOfBirth],[PartTime],[Salaried],[AddressLine1],[AddressLine2]
		,[City],[PostalCode],[LocalNumber],[OtherNumber],[Email],[EmergencyName],[EmergencyNumber],[ContactRelation]
		,[Gender],[HireDate],[OriginalHireDate],[SalaryStartDate],[HourlySalary],[AnnualSalary],[ClassSequence]
		,[BoClassSequence], [LastUpdatedDate], [RevenueCenterSeq], [TerminationNote], [TerminationDate]
		,[Status], [UpdateRecord], [EmployeeSequence]
	FROM [dbo].[EmployeesLive]
	WHERE LocationId = @LocationNumber
	
	OPEN CurEmployeeUpdate
	FETCH NEXT FROM CurEmployeeUpdate
	INTO  
		@LocationId, @EmployeeId, @FirstName, @LastName, 
		@ObjectNumber, @LongFirstName, @LongLastName, @MiddleName,
		@CheckName, @CheckName2, @EffectiveFrom, @DateOfBirth,
		@PartTime, @Salaried, @AddressLine1, @AddressLine2,
		@City, @PostalCode, @LocalNumber, @OtherNumber, 
		@Email, @EmergencyName, @EmergencyNumber, @ContactRelation,
		@Gender, @HireDate, @OriginalHireDate, @SalaryStartDate, 
		@HourlySalary, @AnnualSalary, @ClassSequence,
		@BoClassSequence, @LastUpdatedDate, @RevenueCenterSeq, 
		@TerminationDate, @TerminationNote, @Status, @UpdateRecord, @EmployeeSequence
	WHILE @@FETCH_STATUS = 0
	BEGIN 
			
		IF  @UpdateRecord = 1 AND @EmployeeSequence <> 'NULL'
		BEGIN
		
			TRUNCATE TABLE [dbo].[MicrosEmployeesJobs]
			
			select @MyStr = 'ALTER PROCEDURE [dbo].[spDynamicSqlDownloadMicrosEmployeesJobs]' + Char(13)
			select @MyStr = @MyStr + ' AS '  + Char(13)
			select @MyStr = @MyStr + ' BEGIN'  + Char(13)
			select @MyStr3 = ' END'

			

			select @MyStr2 = 'INSERT INTO [dbo].[MicrosEmployeesJobs] (EmployeeSequence, JobSequence)' + CHAR(13)
			select @MyStr2 = @MyStr2 + 'SELECT emp_seq, job_seq FROM OPENQUERY(' + @LinkedServer +  ', ' + CHAR(13) 
			select @MyStr2 = @MyStr2 + '''SELECT emp_seq, job_seq FROM micros.job_rate_def WHERE emp_seq =' + @EmployeeSequence + ''') ' + Char(13)

														
			select @MyStrAll = @MyStr+@MyStr2+@MyStr3
			--PRINT @MyStrAll + CHAR(13)
			
			BEGIN TRY
				exec (@MyStrAll)
				EXEC spDynamicSqlDownloadMicrosEmployeesJobs 
			
			END TRY
			BEGIN CATCH					
				
				DECLARE @Error1 VARCHAR(MAX)
									
				SET @Error1 =  CAST(ERROR_NUMBER() as varchar(32)) 
					+ ' ' + CAST(ERROR_SEVERITY() as varchar(32))
					+ ' ' + CAST(ERROR_STATE() as varchar(32))
					+ ' ' + CAST(ERROR_PROCEDURE() as varchar(256))
					+ ' ' + CAST(ERROR_LINE() as varchar(32))
					+ ' ' + CAST(ERROR_MESSAGE() as varchar(4000))

				PRINT '		spDynamicSqlDownloadMicrosEmployeesJobs failed:' + @Error1 + CHAR(13)
				
				INSERT INTO [dbo].[Logs] (Date, Output, StoredProcedure, Location, DynamicSql)
				SELECT GETDATE(), @Error1, '[dbo].[sp_UBP_Micros_Upsert_Job_Update]', @LocationName, @MyStrAll
				
	
			END CATCH	

			DECLARE CurEmployeeJobUpdate CURSOR FOR
			SELECT 
			   J.JobId, J.Name, J.OverrideRegPayRate, J.RateEffectiveDateTime, J.PrimaryJob, J.JobSequence
			FROM [dbo].[MicrosEmployeesJobs] MEJ 
			INNER JOIN [dbo].[JobsLive] J ON MEJ.EmployeeSequence = J.EmployeeSequence

			OPEN CurEmployeeJobUpdate
			FETCH NEXT FROM CurEmployeeJobUpdate
			INTO @JobId, @Name, @OverrideRegPayRate, @RateEffectiveDateTime, @PrimaryJob, @JobSequence

			WHILE @@FETCH_STATUS = 0
			BEGIN			
			
				--PRINT '				Updating employeeID ' + @employeeID + ' job_seq ' + @JobSequence + ' ...' + CHAR(13)
				
				select @MyStr = 'ALTER PROCEDURE [dbo].[spDynamicSqlEmployeeJobUpdate]' + Char(13)
				select @MyStr = @MyStr + 'AS '  + Char(13)
				select @MyStr = @MyStr + 'BEGIN'  + Char(13)
				select @MyStr3 = ' END'

				select @MyStr2 = '	UPDATE OPENQUERY(' + @LinkedServer + ',''SELECT' + Char(13)
				select @MyStr2 = @MyStr2 + 'E.emp_seq, E.job_seq, E.override_reg_pay_rate, E.rate_effective_datetime, E.last_updated_date, E.ob_primary_job' + Char(13)
				select @MyStr2 = @MyStr2 + 'FROM micros.job_rate_def E WHERE E.emp_seq = ' + @EmployeeSequence + ' AND E.job_seq = ' + @JobSequence  + ''')' + Char(13)
				select @MyStr2 = @MyStr2 + 'SET override_reg_pay_rate = ' + LEFT(@OverrideRegPayRate,5) + ',' + Char(13)
				select @MyStr2 = @MyStr2 + 'rate_effective_datetime = ''' +  @RateEffectiveDateTime + ''',' + Char(13)
				select @MyStr2 = @MyStr2 + 'ob_primary_job = ''' +  CASE WHEN @PrimaryJob = '1' THEN 'T' ELSE 'F' END + ''',' + Char(13)
				select @MyStr2 = @MyStr2 + 'last_updated_date = ''' +  convert(varchar, getdate(), 126) + '''' + Char(13)
				select @MyStrAll = @MyStr+@MyStr2+@MyStr3
				print @MyStrAll

				BEGIN TRY
							
					exec (@MyStrAll);
					Exec spDynamicSqlEmployeeJobUpdate					 		
				
				END TRY
				BEGIN CATCH
									
					DECLARE @Error VARCHAR(MAX)
										
					SET @Error =  CAST(ERROR_NUMBER() as varchar(32)) 
						+ ' ' + CAST(ERROR_SEVERITY() as varchar(32))
						+ ' ' + CAST(ERROR_STATE() as varchar(32))
						+ ' ' + CAST(ERROR_PROCEDURE() as varchar(256))
						+ ' ' + CAST(ERROR_LINE() as varchar(32))
						+ ' ' + CAST(ERROR_MESSAGE() as varchar(4000))

					PRINT '		spUpdateEmployeeJobs failed:' + @Error + CHAR(13)
					
					INSERT INTO [dbo].[Logs] ([Date], [Output], [StoredProcedure], [Location], [DynamicSql])
					SELECT GETDATE(), @Error, '[dbo].[spUpdateEmployeeJobs]', @LocationName, @MyStrAll
					
										
				END CATCH
				
				FETCH NEXT FROM CurEmployeeJobUpdate
				INTO @JobId, @Name, @OverrideRegPayRate, @RateEffectiveDateTime, @PrimaryJob, @JobSequence

			END
			
			CLOSE CurEmployeeJobUpdate
			DEALLOCATE CurEmployeeJobUpdate					
		
		END
			
		FETCH NEXT FROM CurEmployeeUpdate
		INTO  
				@LocationId, @EmployeeId, @FirstName, @LastName, 
				@ObjectNumber, @LongFirstName, @LongLastName, @MiddleName,
				@CheckName, @CheckName2, @EffectiveFrom, @DateOfBirth,
				@PartTime, @Salaried, @AddressLine1, @AddressLine2,
				@City, @PostalCode, @LocalNumber, @OtherNumber, 
				@Email, @EmergencyName, @EmergencyNumber, @ContactRelation,
				@Gender, @HireDate, @OriginalHireDate, @SalaryStartDate, 
				@HourlySalary, @AnnualSalary, @ClassSequence,
				@BoClassSequence, @LastUpdatedDate, @RevenueCenterSeq, 
				@TerminationDate, @TerminationNote, @Status, @UpdateRecord,
				@EmployeeSequence
	END


	CLOSE CurEmployeeUpdate
	DEALLOCATE CurEmployeeUpdate

	


END














