﻿CREATE TABLE [dbo].[SyufyLocations] (
    [Id]             INT           IDENTITY (1, 1) NOT NULL,
    [LocationName]   VARCHAR (50)  NULL,
    [LocationNumber] VARCHAR (50)  NULL,
    [Active]         BIT           NULL,
    [LinkedServer]   VARCHAR (100) NULL,
    CONSTRAINT [PK_SyufyLocations] PRIMARY KEY CLUSTERED ([Id] ASC)
);

