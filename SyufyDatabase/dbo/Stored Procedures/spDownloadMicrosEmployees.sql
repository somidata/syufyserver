﻿


-- =============================================
-- Author:		SomiData
-- Create date: 4/7/2016
-- Description:	Retrieves employee primary key from Micros and update in local table.
-- =============================================
CREATE PROCEDURE [dbo].[spDownloadMicrosEmployees]
	@LocationName varchar(100) = NULL, @LocationNumber varchar(50) = NULL, 
	@LinkedServer  varchar(100) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @MyStr varchar(Max), @MyStr2 varchar(Max), @MyStr3 varchar(Max), @MyStrAll varchar(Max), @MyStr5 varchar(Max)
	
	TRUNCATE TABLE dbo.MicrosEmployees
						
	select @MyStr = 'ALTER PROCEDURE [dbo].[spDynamicSqlDownloadMicrosEmployees]' + Char(13)
	select @MyStr = @MyStr + 'AS '  + Char(13)
	select @MyStr = @MyStr + 'BEGIN'  + Char(13)
	select @MyStr3 = ' END'

	select @MyStr2 = 'INSERT INTO dbo.MicrosEmployees (EmployeeSequence, ObjectNumber, LocationNumber) ' + Char(13)
	select @MyStr2 = @MyStr2 + 'SELECT emp_seq, obj_num, ''' + @LocationNumber + '''' + Char(13)
	select @MyStr2 = @MyStr2 + 'FROM OPENQUERY(' + @LinkedServer + ',''SELECT' + Char(13)	
	select @MyStr2 = @MyStr2 + 'emp_seq, obj_num ' + Char(13)
	select @MyStr2 = @MyStr2 + 'FROM micros.emp_def '')' + Char(13)
	select @MyStrAll = @MyStr+@MyStr2+@MyStr3

	BEGIN TRY
	
		exec (@MyStrAll);
		--print @MyStrAll
		Exec spDynamicSqlDownloadMicrosEmployees				 					
		
		UPDATE E 
		SET E.EmployeeSequence = M.EmployeeSequence
		FROM [dbo].[EmployeesLive] E
		INNER JOIN [dbo].[MicrosEmployees] M ON
		CAST( E.EmployeeId AS INT) = CAST(M.ObjectNumber AS INT) AND
		M.LocationNumber = E.LocationId
		WHERE E.LocationId = @LocationNumber

		UPDATE J
		SET J.EmployeeSequence = E.EmployeeSequence
		FROM [dbo].[JobsLive] J
		INNER JOIN [dbo].[EmployeesLive] E ON
		CAST( E.EmployeeId AS INT) = CAST(J.EmployeeId AS INT) 
		WHERE E.LocationId = @LocationNumber
	
	END TRY
	BEGIN CATCH					
		
		DECLARE @Error VARCHAR(MAX)
							
		SET @Error =  CAST(ERROR_NUMBER() as varchar(32)) 
			+ ' ' + CAST(ERROR_SEVERITY() as varchar(32))
			+ ' ' + CAST(ERROR_STATE() as varchar(32))
			+ ' ' + CAST(ERROR_PROCEDURE() as varchar(256))
			+ ' ' + CAST(ERROR_LINE() as varchar(32))
			+ ' ' + CAST(ERROR_MESSAGE() as varchar(4000))
		
		INSERT INTO [dbo].[Logs] ([Date], [Output], [StoredProcedure], [Location])
		SELECT GETDATE(), @Error,'[dbo].[spDownloadMicrosEmployees]', @LocationName
		
		PRINT '		spDownloadMicrosEmployees failed:' + @Error + CHAR(13)

	END CATCH		



END











