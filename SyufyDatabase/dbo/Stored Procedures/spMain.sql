﻿

-- =============================================
-- Author:		SomiData
-- Create date: 4/7/2016
-- Description:	Loads Syufy Employee data into POS.
-- =============================================
CREATE PROCEDURE [dbo].[spMain]
@IncrementalUpdate int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	BEGIN TRY		
		
		PRINT 'Begin..........' + CHAR(13)

		DECLARE @CurrentTimeStamp DATETIME = GETDATE(), @ErrorCount INT = 0, @MainError VARCHAR(MAX),
		@IsRunning INT = 0, @ReturnValue INT = 0

		-- Empty the live tables.
		EXEC spEmptyLiveTables

		-- Load the live tables.
		EXEC spLoadLiveTables

		-- Load historical tables
		EXEC spLoadHistoricalTables

		-- Upsert Syufy employee data into Micros POS.
		EXEC spUpsertIntoMicrosPos

		-- Empty the staging tables.
		EXEC spEmptyStagingTables

		SELECT @ErrorCount = COUNT(*) FROM [dbo].[Logs]	WHERE [Date] >= @CurrentTimeStamp
		IF @ErrorCount >= 1
		 BEGIN
		 	
		 	SELECT @MainError = (SELECT TOP(1) [Output] FROM [dbo].[Logs] WHERE [Date] >= @CurrentTimeStamp)
			RAISERROR (@MainError, 16, 1);
		 END

		PRINT 'End..........' + CHAR(13)
	
	END TRY
	BEGIN CATCH


		-- Empty the staging tables.
		EXEC spEmptyStagingTables
		
		PRINT 'Completed with error..........' + CHAR(13)
			
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();			


		DECLARE @Error VARCHAR(MAX)
							
		SET @Error =  CAST(ERROR_NUMBER() as varchar(32)) 
			+ ' ' + CAST(ERROR_SEVERITY() as varchar(32))
			+ ' ' + CAST(ERROR_STATE() as varchar(32))
			+ ' ' + CAST(ERROR_PROCEDURE() as varchar(256))
			+ ' ' + CAST(ERROR_LINE() as varchar(32))
			+ ' ' + CAST(ERROR_MESSAGE() as varchar(4000))

		PRINT 'spMain failed:' + @Error + CHAR(13)
		
		INSERT INTO [dbo].[Logs] ([Date], [Output], [StoredProcedure])
		SELECT GETDATE(), @Error, '[dbo].[spMain]'
						
		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);	
					
	END CATCH		
	
END




