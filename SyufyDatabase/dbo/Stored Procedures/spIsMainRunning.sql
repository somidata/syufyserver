﻿

-- =============================================
-- Author:		SomiData
-- Create date: 4/11/2016
-- Description:	Determines if spMain is running.
-- =============================================
CREATE PROCEDURE [dbo].[spIsMainRunning]
--@IsRunning int output 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	BEGIN TRY
		
		PRINT 'Begin..........' + CHAR(13)

		DECLARE @spName NVARCHAR(MAX)    -- the string we're looking for. The variable to become a parameter if you wish to extend this sp
		DECLARE @handle SMALLINT    -- the spid of the process
		DECLARE @sql NVARCHAR(MAX)  -- the dynamic SQL
		DECLARE @table TABLE ( EventType nvarchar(30) , [Parameters] int , EventInfo nvarchar(4000) )   -- the table variable holding the result of DBCC INPUTBUFFER execution

		DECLARE @tblProcs as TABLE (i int identity, spName nvarchar(100),IsRunning BIT) 
		DECLARE @i int
		SET @i = 1

		INSERT INTO @tblProcs (spName, IsRunning) SELECT name, 0 as IsRunning FROM dbo.sysobjects WHERE (type = 'P')

		DECLARE procs CURSOR FOR SELECT session_id FROM sys.dm_exec_requests WHERE status IN ('running', 'suspended', 'pending', 'runnable') AND session_id <> @@SPID ORDER BY session_id DESC  -- these are the processes to examine

		OPEN procs
		FETCH NEXT FROM procs INTO @handle
		WHILE @@FETCH_STATUS=0 
		BEGIN

		WHILE @i <= (Select count(*) from @tblProcs)
		BEGIN

			SELECT @spName = spName FROM @tblProcs WHERE i=@i

			BEGIN TRY            
				DELETE FROM @table

				SET @sql = 'DBCC INPUTBUFFER(' + CAST(@handle AS NVARCHAR) + ')'
                
				INSERT INTO @table
				EXEC (@sql)

				SELECT @sql = EventInfo FROM @table
			END TRY
			BEGIN CATCH
				SET @sql = ''
			END CATCH
        
			IF CHARINDEX( @spName, @sql, 0 ) > 0
			BEGIN
				UPDATE @tblProcs SET IsRunning = IsRunning + 1 WHERE spName = @spName
			END

			SET @i = @i + 1
			END

			FETCH NEXT FROM procs INTO @handle
		END
		CLOSE procs DEALLOCATE procs

		--SET @IsRunning =  (CASE WHEN EXISTS ( SELECT spName FROM @tblProcs 
		--									WHERE 
		--									(spName = 'spEmptyLiveTables' AND IsRunning = 1) OR
		--									(spName = 'spLoadLiveTables' AND IsRunning = 1) OR
		--									(spName = 'spLoadHistoricalTables' AND IsRunning = 1) OR
		--									(spName = 'spUpsertIntoMicrosPos' AND IsRunning = 1) OR
		--									(spName = 'spEmptyStagingTables' AND IsRunning = 1) 
		--				) THEN 1 ELSE 0 END) 
		
		SELECT COUNT(*) AS [Status] FROM @tblProcs WHERE spName = 'spMain' AND IsRunning = 1

		PRINT 'End..........' + CHAR(13)
	
	END TRY
	BEGIN CATCH
		
		PRINT 'Completed with error..........' + CHAR(13)
		
		PRINT 
			'		' + CAST(ERROR_NUMBER() as varchar(32)) 
			+ ' ' + CAST(ERROR_SEVERITY() as varchar(32))
			+ ' ' + CAST(ERROR_STATE() as varchar(32))
			+ ' ' + CAST(ERROR_PROCEDURE() as varchar(256))
			+ ' ' + CAST(ERROR_LINE() as varchar(32))
			+ ' ' + CAST(ERROR_MESSAGE() as nvarchar(4000))
			+ CHAR(13)
			
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();			


		DECLARE @Error VARCHAR(MAX)
							
		SET @Error =  CAST(ERROR_NUMBER() as varchar(32)) 
			+ ' ' + CAST(ERROR_SEVERITY() as varchar(32))
			+ ' ' + CAST(ERROR_STATE() as varchar(32))
			+ ' ' + CAST(ERROR_PROCEDURE() as varchar(256))
			+ ' ' + CAST(ERROR_LINE() as varchar(32))
			+ ' ' + CAST(ERROR_MESSAGE() as varchar(4000))
		
		INSERT INTO [dbo].[Logs] ([Date], [Output], [StoredProcedure])
		SELECT GETDATE(), @Error, '[dbo].[spIsMainRunning]'
		
		PRINT '		' + @Error + CHAR(13)
						
		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);	
					
	END CATCH		
	
END




