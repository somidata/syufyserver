﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;

namespace SyufyClient
{
    class Program
    {
        private static string baseUri = WebConfigurationManager.AppSettings["baseUri"];

        static void Main(string[] args)
        {
            try
            {

                //Console.WriteLine(String.Format("{0}", AppDomain.CurrentDomain.BaseDirectory));
                //Console.ReadLine();

                string userName = "craig_tottie@150pelican.com";
                string password = "Syufy16$";

                //var registerResult = Register(userName, password);
                //Console.WriteLine("Registration Status Code: {0}", registerResult);

                Dictionary<string, string> token = GetTokenDictionary(userName, password);
                Console.WriteLine("");
                Console.WriteLine("Access Token:");

                //Write each item in the dictionary out to the console:
                foreach (var kvp in token)
                {
                    Console.WriteLine("{0}: {1}", kvp.Key, kvp.Value);
                }

                //Console.WriteLine("");
                //Console.WriteLine("Getting User Info:");
                //Console.WriteLine(GetUserInfo(token["access_token"]));


                Console.WriteLine("");
                Console.WriteLine("Setting Employee Info:");
                Console.WriteLine(SetEmployees(token["access_token"]));


            }
            catch (Exception e)
            {
                Console.WriteLine(String.Format(e.ToString()));
            }
        }

        static HttpClient CreateClient(string accessToken = "")
        {
            var client = new HttpClient();
            if (!string.IsNullOrWhiteSpace(accessToken))
            {
                client.DefaultRequestHeaders.Authorization =
                    new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", accessToken);
            }
            return client;
        }

        static string Register(string email, string password)
        {
            string uri = baseUri + "api/Account/Register";

            var registerModel = new
            {
                Email = email,
                Password = password,
                ConfirmPassword = password
            };
            using (var client = new HttpClient())
            {
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                var response =
                    client.PostAsJsonAsync(uri,registerModel).Result;
                return response.StatusCode.ToString();
            }
        }


        static string GetToken(string userName, string password)
        {
            string uri = baseUri + "/Token";

            var pairs = new List<KeyValuePair<string, string>>
                        {
                            new KeyValuePair<string, string>( "grant_type", "password" ),
                            new KeyValuePair<string, string>( "username", userName ),
                            new KeyValuePair<string, string> ( "Password", password )
                        };
            var content = new FormUrlEncodedContent(pairs);
            using (var client = new HttpClient())
            {
                var response =
                    client.PostAsync(uri, content).Result;
                return response.Content.ReadAsStringAsync().Result;
            }
        }

        static Dictionary<string, string> GetTokenDictionary(string userName, string password)
        {
            string uri = baseUri + "Token";

            var pairs = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>( "grant_type", "password" ),
                    new KeyValuePair<string, string>( "username", userName ),
                    new KeyValuePair<string, string> ( "Password", password )
                };


            var content = new FormUrlEncodedContent(pairs);

            using (var client = new HttpClient())
            {
                //ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                var response = client.PostAsync(uri, content).Result;
                var result = response.Content.ReadAsStringAsync().Result;


                Console.WriteLine("");
                Console.WriteLine(String.Format("{0}", result.ToString()));
                Console.WriteLine("");

                // Deserialize the JSON into a Dictionary<string, string>
                Dictionary<string, string> tokenDictionary =
                    JsonConvert.DeserializeObject<Dictionary<string, string>>(result);
                return tokenDictionary;
            }
        }

        static string GetUserInfo(string token)
        {
            string uri = baseUri + "api/Account/UserInfo";
            using (var client = CreateClient(token))
            {
                var response = client.GetAsync(uri).Result;
                return response.Content.ReadAsStringAsync().Result;
            }
        }

        static string SetEmployees(string token)
        {
            
            var employeeModelArray = new[]
            {
            new
            {
                LocationId = 1,
                EmployeeId = 9995843,
                FirstName = "John",
                LastName = "Doe",
                LongFirstName = "John",
                LongLastName = "Doe",
                MiddleName = "JD",
                CheckName = "John Doe",
                CheckName2 = "John Doe",
                EffectiveFrom = Convert.ToDateTime("2/3/2016"),
                DateOfBirth = Convert.ToDateTime("7/5/2016"),
                PartTime = false,
                Salaried = false,
                AddressLine1 = "300 S. Raymond",
                AddressLine2 = "N/A",
                City = "Pasadena",
                PostalCode = 11111,
                LocalNumber = "1115556666",
                OtherNumber = "7778889999",
                Email = "john@gmail.com",
                EmergencyName = "Jane",
                EmergencyNumber = "1112223333",
                ContactRelation = "Relative",
                Gender = "Male",
                HireDate = Convert.ToDateTime("4/3/2016"),
                OriginalHireDate = Convert.ToDateTime("3/3/2016"),
                SalaryStartDate = Convert.ToDateTime("5/3/2016"),
                HourlySalary = 10.00,
                AnnualSalary = 10.00,
                TerminationDate = (DateTime?) null,
                TerminationNote = "She's back",
                PasswordId = 0007843,
                DefaultTemplateId = 1,
                PayrollId = 4873,
                Jobs = new[]
                {
                    new
                    {
                        JobId = 602,
                        Name = "Prep Cook",
                        OverrideRegPayRate = 1.00,
                        RateEffectiveDatetime = Convert.ToDateTime("2/1/1983"),
                        PrimaryJob = true,
                        DefaultTemplateId = 1
                    },
                    new
                    {
                        JobId = 603,
                        Name = "Dishwater",
                        OverrideRegPayRate = 2.00,
                        RateEffectiveDatetime = Convert.ToDateTime("2/1/1977"),
                        PrimaryJob = true,
                        DefaultTemplateId = 1
                    }

                }
            },
            new
            {
                LocationId = 1,
                EmployeeId = 9998944,
                FirstName = "Jane",
                LastName = "Doe",
                LongFirstName = "Jane",
                LongLastName = "Doe",
                MiddleName = "JD",
                CheckName = "Jane Doe",
                CheckName2 = "Jane Doe",
                EffectiveFrom = Convert.ToDateTime("2/3/2016"),
                DateOfBirth = Convert.ToDateTime("11/7/2001"),
                PartTime = false,
                Salaried = false,
                AddressLine1 = "300 S. Raymond",
                AddressLine2 = "N/A",
                City = "Pasadena",
                PostalCode = 22222,
                LocalNumber = "1112223333",
                OtherNumber = "111222333",
                Email = "jane@gmail.com",
                EmergencyName = "John",
                EmergencyNumber = "2221114444",
                ContactRelation = "Relative",
                Gender = "Female",
                HireDate = Convert.ToDateTime("5/3/1999"),
                OriginalHireDate = Convert.ToDateTime("1/3/1998"),
                SalaryStartDate = Convert.ToDateTime("2/3/1998"),
                HourlySalary = 50.00,
                AnnualSalary = 0.0,
                TerminationDate = (DateTime?) null,
                TerminationNote = "He's here.",
                PasswordId = 0003944,
                DefaultTemplateId = 1,
                PayrollId = 9998,
                Jobs = new[]
                {
                    new
                    {
                        JobId = 601,
                        Name = "Line Cook",
                        OverrideRegPayRate = 3.00,
                        RateEffectiveDatetime = Convert.ToDateTime("7/1/1999"),
                        PrimaryJob = true,
                        DefaultTemplateId = 1
                    }
                }
            }
            };

            string uri = baseUri + "api/Employee/SetEmployees";

            using (var client = CreateClient(token))
            {
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                var response =
                        client.PostAsJsonAsync(
                            uri,
                            employeeModelArray).Result;
                return response.StatusCode.ToString();
            }

        }

        private static void WebClient()
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://localhost:52046/Token");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            //httpWebRequest.Headers.Add("grant_type:password");
            //httpWebRequest.Headers.Add("username:jose@somidata.com");
            //httpWebRequest.Headers.Add("password:Syufy2016$");


            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = "{\"grant_type\":\"password\"," +
                              "{\"username\":\"jose@somidata.com\"," +
                              "\"password\":\"Syufy2016$\"}";

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                var response = result;
            }
        }


    }
}
