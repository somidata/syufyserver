﻿CREATE TABLE [dbo].[MicrosSecurityLevelDefinition] (
    [EmployeeClassSequence] VARCHAR (250) NULL,
    [ObjectNumber]          INT           NULL,
    [Name]                  VARCHAR (250) NULL,
    [LocationNumber]        VARCHAR (50)  NULL
);

