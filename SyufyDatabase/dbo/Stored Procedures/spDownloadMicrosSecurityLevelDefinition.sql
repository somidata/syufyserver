﻿



-- =============================================
-- Author:		SomiData
-- Create date: 4/8/2016
-- Description:	Retrieves security level definition from Micros and update in local table.
-- =============================================
CREATE PROCEDURE [dbo].[spDownloadMicrosSecurityLevelDefinition]
	@LocationName varchar(100) = NULL, @LocationNumber varchar(50) = NULL, 
	@LinkedServer  varchar(100) = NULL
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @MyStr varchar(Max), @MyStr2 varchar(Max), @MyStr3 varchar(Max), @MyStrAll varchar(Max), @MyStr5 varchar(Max)
	
	TRUNCATE TABLE dbo.MicrosSecurityLevelDefinition
						
	select @MyStr = 'ALTER PROCEDURE [dbo].[spDynamicSqlDownloadMicrosSecurityLevelDefinition] ' + Char(13)
	select @MyStr = @MyStr + 'AS '  + Char(13)
	select @MyStr = @MyStr + 'BEGIN '  + Char(13)
	select @MyStr3 = ' END '

	select @MyStr2 = 'INSERT INTO dbo.MicrosSecurityLevelDefinition (EmployeeClassSequence, ObjectNumber, Name, LocationNumber) ' + Char(13)
	select @MyStr2 = @MyStr2 + 'SELECT emp_class_seq, obj_num, name, ''' + @LocationNumber + '''' + Char(13)
	select @MyStr2 = @MyStr2 + ' FROM OPENQUERY(' + @LinkedServer + ',''SELECT ' + Char(13)	
	select @MyStr2 = @MyStr2 + 'emp_class_seq, obj_num, name ' + Char(13)
	select @MyStr2 = @MyStr2 + 'FROM micros.emp_class_def '')' + Char(13)
	select @MyStrAll = @MyStr+@MyStr2+@MyStr3

	BEGIN TRY

		--print @MyStrAll
		Exec (@MyStrAll);
		Exec spDynamicSqlDownloadMicrosSecurityLevelDefinition	
		
		UPDATE E
		SET E.ClassSequence= S.EmployeeClassSequence
		FROM [dbo].[EmployeesLive] E
		INNER JOIN [dbo].[MicrosSecurityLevelDefinition] S ON E.LocationId = S.LocationNumber
		INNER JOIN [dbo].[DefaultEmployeeTemplate] T ON E.LocationId = T.LocationId
		AND E.DefaultTemplateId = T.DefaultTemplateId
		AND T.ClassSequenceName = S.Name
		AND E.LocationId = @LocationNumber
		

	END TRY
	BEGIN CATCH			
		
		DECLARE @Error VARCHAR(MAX)
							
		SET @Error =  CAST(ERROR_NUMBER() as varchar(32)) 
			+ ' ' + CAST(ERROR_SEVERITY() as varchar(32))
			+ ' ' + CAST(ERROR_STATE() as varchar(32))
			+ ' ' + CAST(ERROR_PROCEDURE() as varchar(256))
			+ ' ' + CAST(ERROR_LINE() as varchar(32))
			+ ' ' + CAST(ERROR_MESSAGE() as varchar(4000))
		
		INSERT INTO [dbo].[Logs] ([Date], [Output], [StoredProcedure], [Location])
		SELECT GETDATE(), @Error, '[dbo].[spDownloadMicrosSecurityLevelDefinition]', @LocationName
		
		PRINT '		spDownloadMicrosSecurityLevelDefinition failed:' + @Error + CHAR(13)

	END CATCH		

END
















