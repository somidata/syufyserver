﻿using System.Data.Entity;
using SyufyServer.Data;

namespace SyufyServer
{
    public class SyufyDb : DbContext
    {

        public SyufyDb()
            : base("DefaultConnection")
        {

        }

        //Adding custom tables to exsiting DbContext
        public DbSet<SyufyUsers> SyufyUser { get; set; }

        public DbSet<Employees> Employee { get; set; }

        public DbSet<Jobs> Job { get; set; }

        public static SyufyDb Create()
        {
            return new SyufyDb();
        }

    }
}