﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using SyufyServer.Helper;
using SyufyServer.Models;

namespace SyufyServer.Controllers
{
    [Authorize]
    [RoutePrefix("api/Employee")]
    public class EmployeeController : ApiController
    {
        private const string LocalLoginProvider = "Local";
        private ApplicationUserManager _userManager;


        public EmployeeController()
        {
        }

        public SyufyDb SyufyDbContext { get; set; }

        public EmployeeController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }

        /// <summary>
        /// Loads the employee and job data into dbo.Employees and dbo.Jobs respectively
        /// and executes spMain.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        // POST api/Employee/SetEmployees
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("SetEmployees")]
        public IHttpActionResult SetEmployeesInfo(SetEmployeeBindingModel[] model)
        {
            SyufyDbContext = SyufyDb.Create();
            int entitiesToAddCount = 0, save = 0;

            try
            {

                // Check if the submitted data is valid
                if (!ModelState.IsValid)
                {
                    Log.WriteToLog("Bad Request", "Model state is invalid.");
                    return BadRequest(ModelState);
                }

                // Check if stored procedure that loads Micros DB is currently running.
                List<StoredProcedureStatus> Status = SyufyDbContext.Database.SqlQuery<StoredProcedureStatus>("spIsMainRunning").ToList();

                if (Status.Count() > 0 && Status[0].Status == 1)
                {
                    Log.WriteToLog("Bad Request", "Stored procedure spMain is running.");
                    return BadRequest("Internal Server Error");
                }

                // Insert employee records into dbo.Employee
                foreach (var employee in model)
                {

                    var entityEmployee = new Data.Employees()
                    {
                        LocationId = employee.LocationId,
                        EmployeeId = employee.EmployeeId,
                        FirstName = employee.FirstName,
                        LastName = employee.LastName,
                        LongFirstName = employee.LongFirstName,
                        LongLastName = employee.LongLastName,
                        MiddleName = employee.MiddleName,
                        CheckName = employee.CheckName,
                        CheckName2 = employee.CheckName2,
                        EffectiveFrom = employee.EffectiveFrom,
                        DateOfBirth = employee.DateOfBirth,
                        PartTime = employee.PartTime,
                        Salaried = employee.Salaried,
                        AddressLine1 = employee.AddressLine1,
                        AddressLine2 = employee.AddressLine2,
                        City = employee.City,
                        PostalCode = employee.PostalCode,
                        LocalNumber = employee.LocalNumber,
                        OtherNumber = employee.OtherNumber,
                        Email = employee.Email,
                        EmergencyName = employee.EmergencyName,
                        EmergencyNumber = employee.EmergencyNumber,
                        ContactRelation = employee.ContactRelation,
                        Gender = employee.Gender,
                        HireDate = employee.HireDate,
                        OriginalHireDate = employee.OriginalHireDate,
                        SalaryStartDate = employee.SalaryStartDate,
                        HourlySalary = employee.HourlySalary,
                        AnnualSalary = employee.AnnualSalary,
                        TerminationDate = employee.TerminationDate,
                        TerminationNote = employee.TerminationNote,
                        DefaultTemplateId = employee.DefaultTemplateId,
                        PayrollId = employee.PayrollId,
                        PasswordId = employee.PasswordId
                    };

                    SyufyDbContext.Employee.Add(entityEmployee);

                    // Insert job records into dbo.Job
                    foreach (var job in employee.Jobs)
                    {

                        var entityJob = new Data.Jobs()
                        {
                            EmployeeId = employee.EmployeeId,
                            JobId = job.JobId,
                            Name = job.Name,
                            OverrideRegPayRate = job.OverrideRegPayRate,
                            RateEffectiveDatetime = job.RateEffectiveDatetime,
                            PrimaryJob = job.PrimaryJob,
                            DefaultTemplateId = job.DefaultTemplateId
                        };

                        SyufyDbContext.Job.Add(entityJob);

                        entitiesToAddCount++;
                    }

                    entitiesToAddCount++;

                    // Insert at most 1000 entities at time for improved performance 
                    if (entitiesToAddCount == 10000)
                    {
                        entitiesToAddCount = 0;

                        save = SyufyDbContext.SaveChanges();

                        if (SyufyDbContext != null)
                        {
                            SyufyDbContext.Dispose();
                            SyufyDbContext = SyufyDb.Create();
                        }

                    }

                }

                save = SyufyDbContext.SaveChanges();

                // Check if stored procedure that loads Micros DB is currently running.
                Status = SyufyDbContext.Database.SqlQuery<StoredProcedureStatus>("spIsMainRunning").ToList();


                if (Status.Count() > 0 && Status[0].Status == 1)
                {
                    Log.WriteToLog("Bad Request", "Stored procedure spMain is running.");
                    return BadRequest("Internal Server Error");
                }

                // Execute stored procedure to load Micros POS
                // Start the child process.
                Process p = new Process();
                // Redirect the output stream of the child process.
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.FileName = AppDomain.CurrentDomain.BaseDirectory +  "SyufyExecuteSql.exe";
                p.Start();
                // Do not wait for the child process to exit before
                // reading to the end of its redirected stream.
                // p.WaitForExit();
                // Read the output stream first and then wait.
                string output = p.StandardOutput.ReadToEnd();
                p.WaitForExit();

                if (output.Contains("EXCEPTION"))
                {
                    Log.WriteToLog("Bad Request", output);
                    Log.SendEmail(String.Format("EXCEPTION: {0}", output));
                    return BadRequest("Internal Server Error");
                }

                return Ok();
            }
            catch (Exception e)
            {
                Log.SendEmail(String.Format("EXCEPTION: {0}", e.ToString()));

                Log.WriteToLog("EXCEPTION", e.ToString());

            }
            finally
            {
                if (SyufyDbContext != null)
                {
                    SyufyDbContext.Dispose();
                }
            }

            return BadRequest("Internal Server Error");
        }


        #region Helpers


        #endregion
    }
}