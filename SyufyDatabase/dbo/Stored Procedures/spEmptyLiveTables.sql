﻿
-- =============================================
-- Author:		SomiData
-- Create date: 4/7/2016
-- Description:	Empties live tables.
-- =============================================
CREATE PROCEDURE [dbo].[spEmptyLiveTables]


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	TRUNCATE TABLE [dbo].[EmployeesLive]
	TRUNCATE TABLE [dbo].[JobsLive]
	TRUNCATE TABLE [dbo].[MicrosEmployees]	
	TRUNCATE TABLE [dbo].[MicrosSecurityLevelDefinition]
	TRUNCATE TABLE [dbo].[MicrosManagerLevelDefinition]
	TRUNCATE TABLE [dbo].[MicrosRevenueCenterDefinition]
	TRUNCATE TABLE [dbo].[MicrosJobsDefinition]

END



