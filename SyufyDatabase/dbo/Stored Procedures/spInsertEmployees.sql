﻿-- =============================================
-- Author:		SomiData
-- Create date: 4/7/2016
-- Description:	Inserts employee data into Micros POS.
-- =============================================
CREATE PROCEDURE [dbo].[spInsertEmployees]
	@LocationName varchar(100) = NULL, @LocationNumber varchar(50) = NULL, 
	@LinkedServer  varchar(100) = NULL, @IncrementalUpdate int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @MyStr varchar(Max), @MyStr2 varchar(Max), @MyStr3 varchar(Max), @MyStrAll varchar(Max),
			@LocationId varchar(100), @EmployeeId varchar(100), @FirstName varchar(100), @LastName varchar(100), 
			@ObjectNumber varchar(100), @LongFirstName varchar(100), @LongLastName varchar(100), @MiddleName varchar(100),
			@CheckName varchar(100), @CheckName2 varchar(100), @EffectiveFrom varchar(100), @DateOfBirth varchar(100),
			@PartTime varchar(100), @Salaried varchar(100), @AddressLine1 varchar(100), @AddressLine2 varchar(100),
			@City varchar(100), @PostalCode varchar(100), @LocalNumber varchar(100), @OtherNumber varchar(100), 
			@Email varchar(100), @EmergencyName varchar(100), @EmergencyNumber varchar(100), @ContactRelation varchar(100),
			@Gender varchar(100), @HireDate varchar(100), @OriginalHireDate varchar(100), @SalaryStartDate varchar(100), 
			@HourlySalary varchar(100), @AnnualSalary varchar(100), @ClassSequence varchar(100),
			@BoClassSequence varchar(100), @LastUpdatedDate varchar(100), @RevenueCenterSeq varchar(100),
			@TerminationDate varchar(100), @TerminationNote varchar(100), @Status varchar(100), 
			@UpdateRecord varchar(100), @EmployeeExists varchar(100), @ReturnValue varchar(100),
			@PayrollId varchar(100), @PasswordId varchar(100)
			
	DECLARE CurEmployeeUpdate CURSOR FOR

	SELECT
		 [LocationId],[EmployeeId],[FirstName],[LastName],[ObjectNumber],[LongFirstName],[LongLastName],[MiddleName]
		,[CheckName],[CheckName2],[EffectiveFrom],[DateOfBirth],[PartTime],[Salaried],[AddressLine1],[AddressLine2]
		,[City],[PostalCode],[LocalNumber],[OtherNumber],[Email],[EmergencyName],[EmergencyNumber],[ContactRelation]
		,[Gender],[HireDate],[OriginalHireDate],[SalaryStartDate],[HourlySalary],[AnnualSalary],[ClassSequence]
		,[BoClassSequence], [LastUpdatedDate], [RevenueCenterSeq], [TerminationNote], [TerminationDate]
		,[Status], [UpdateRecord], [PayrollId], [PasswordId]
	FROM [dbo].[EmployeesLive]
	WHERE LocationId = @LocationNumber
	
	OPEN CurEmployeeUpdate
	FETCH NEXT FROM CurEmployeeUpdate
	INTO  
		@LocationId, @EmployeeId, @FirstName, @LastName, 
		@ObjectNumber, @LongFirstName, @LongLastName, @MiddleName,
		@CheckName, @CheckName2, @EffectiveFrom, @DateOfBirth,
		@PartTime, @Salaried, @AddressLine1, @AddressLine2,
		@City, @PostalCode, @LocalNumber, @OtherNumber, 
		@Email, @EmergencyName, @EmergencyNumber, @ContactRelation,
		@Gender, @HireDate, @OriginalHireDate, @SalaryStartDate, 
		@HourlySalary, @AnnualSalary, @ClassSequence,
		@BoClassSequence, @LastUpdatedDate, @RevenueCenterSeq, 
		@TerminationDate, @TerminationNote, @Status, @UpdateRecord,
		@PayrollId, @PasswordId

	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @UpdateRecord = 1
		BEGIN 
							
			select @MyStr = 'ALTER PROCEDURE [dbo].[spDynamicSqlEmployeeExists]' + Char(13)
			select @MyStr = @MyStr + ' @EmployeeExists int output '  + Char(13)
			select @MyStr = @MyStr + ' AS '  + Char(13)
			select @MyStr = @MyStr + ' BEGIN'  + Char(13)
			select @MyStr3 = ' END'

				

			select @MyStr2 = ' SET @EmployeeExists = (CASE ' + CHAR(13)
			select @MyStr2 = @MyStr2 + '	WHEN EXISTS (SELECT obj_num FROM OPENQUERY(' + @LinkedServer +  ', ''SELECT TOP(1) obj_num FROM micros.emp_def P WHERE obj_num =' + @employeeID + ''')) THEN 1' + Char(13)
			select @MyStr2 = @MyStr2 + '    ELSE 0 END) ' + CHAR(13)
															
			select @MyStrAll = @MyStr+@MyStr2+@MyStr3


			BEGIN TRY				
				exec (@MyStrAll)
				EXEC @ReturnValue = [dbo].[spDynamicSqlEmployeeExists] @EmployeeExists = @EmployeeExists OUTPUT
			END TRY
			BEGIN CATCH					
				DECLARE @Error1 VARCHAR(MAX)
											
				SET @Error1 =  CAST(ERROR_NUMBER() as varchar(32)) 
					+ ' ' + CAST(ERROR_SEVERITY() as varchar(32))
					+ ' ' + CAST(ERROR_STATE() as varchar(32))
					+ ' ' + CAST(ERROR_PROCEDURE() as varchar(256))
					+ ' ' + CAST(ERROR_LINE() as varchar(32))
					+ ' ' + CAST(ERROR_MESSAGE() as varchar(4000))
					
				PRINT '		spDynamicSqlEmployeeExists failed:' + @Error1 + CHAR(13)												

				INSERT INTO [dbo].[Logs] ([Date], [Output], [StoredProcedure], Location, DynamicSql)
				SELECT GETDATE(), @Error1, '[dbo].[spInsertEmployees]', @LocationName, @MyStrAll
						
	
			END CATCH	

				
			IF @EmployeeExists = 0
			BEGIN 
					
				--PRINT '				Inserting ' + @employeeID + ' ' + @firstName +  ' ' + @lastName + ' ...' + CHAR(13)								
					
				select @MyStr = 'ALTER PROCEDURE [dbo].[spDynamicSqlEmployeeInsert]' + Char(13)
				select @MyStr = @MyStr + 'AS '  + Char(13)
				select @MyStr = @MyStr + 'BEGIN'  + Char(13)
				select @MyStr3 = ' END'
												
				select @MyStr2 = '	INSERT OPENQUERY(' + @LinkedServer + ',''SELECT '
					
				IF @PasswordId <> ''
				BEGIN 
					select @MyStr2 = @MyStr2 + 'E.id, E.last_name, E.first_name, E.obj_num, E.long_last_name, E.long_first_name, E.middle_name, ' + Char(13)
				END
				ELSE				
				BEGIN
					select @MyStr2 = @MyStr2 + 'E.last_name, E.first_name, E.obj_num, E.long_last_name, E.long_first_name, E.middle_name, ' + Char(13)				
				END
				select @MyStr2 = @MyStr2 + 'E.chk_name, E.chk_name2, E.effective_from, E.date_of_birth, E.ob_part_time, ' 
				select @MyStr2 = @MyStr2 + 'E.ob_salaried, E.addr_ln_1, E.addr_ln_2, E.city, E.postal_code, ' + Char(13)
				select @MyStr2 = @MyStr2 + 'E.local_num_1, E.other_local_num, E.e_mail_addr, E.emergency_name,' 
				select @MyStr2 = @MyStr2 + 'E.emergency_number, E.contact_relation, E.gender, E.payroll_id, ' 
				select @MyStr2 = @MyStr2 + 'E.hire_date, E.original_hire_date, E.salary_start_date, ' + Char(13)
					
				IF @BoClassSequence <> '0'
				BEGIN
					select @MyStr2 = @MyStr2 + 'E.hourly_salary, E.annual_salary, E.emp_class_seq, E.last_updated_date, E.emp_bo_class_seq ' 
				END
				ELSE
				BEGIN 
					select @MyStr2 = @MyStr2 + 'E.hourly_salary, E.annual_salary, E.emp_class_seq, E.last_updated_date ' 
				END
					
				select @MyStr2 = @MyStr2 + 'FROM micros.emp_def E '')' + Char(13)
					
				IF @PasswordId <> ''
				BEGIN
					select @MyStr2 = @MyStr2 + 'VALUES (''' + @PasswordId  + ''', ''' +  LEFT(@LastName,16) + ''', ''' +  LEFT(@FirstName,8) + ''', ''' + @EmployeeID + ''', '  
				END
				ELSE
				BEGIN
					select @MyStr2 = @MyStr2 + 'VALUES (''' +  LEFT(@LastName,16) + ''', ''' +  LEFT(@FirstName,8) + ''', ''' + @EmployeeID + ''', '  				
				END
				select @MyStr2 = @MyStr2 + '''' + @LastName + ''', ''' + @FirstName + ''', ''' + @MiddleName + ''', ' 
				select @MyStr2 = @MyStr2 + '''' + LEFT(@FirstName + ' ' + LEFT(@LastName,1),8)+ ''', ''' + LEFT(@FirstName + ' ' + LEFT(@LastName,1),16) + ''', ' + Char(13)
				select @MyStr2 = @MyStr2 + '''' + @EffectiveFrom + ''', ''' + @DateOfBirth + ''', ''' + CASE WHEN @PartTime = '1' THEN 'T' ELSE 'F' END + ''', ' 
				select @MyStr2 = @MyStr2 + '''' +  CASE WHEN @Salaried = '1' THEN 'T' ELSE 'F' END  + ''', ''' + @AddressLine1 + ''', ' 
				select @MyStr2 = @MyStr2 + '''' + @AddressLine2 + ''', ''' + @City + ''', ''' + @PostalCode + ''',' 
				select @MyStr2 = @MyStr2 + '''' + @LocalNumber + ''', ''' + @OtherNumber + ''', ''' + @Email + ''', ''' + @EmergencyName + ''', ' + Char(13)
				select @MyStr2 = @MyStr2 + '''' + @EmergencyNumber + ''', ''' + @ContactRelation + ''', ''' + CASE WHEN LTRIM(RTRIM(LOWER(@Gender))) = 'male' THEN 'M' ELSE 'F' END + ''', ' 
				select @MyStr2 = @MyStr2 + '''' + @PayrollId + ''', ''' +@HireDate + ''', ''' + @OriginalHireDate+ ''', ''' + @SalaryStartDate+ ''', ' 
				select @MyStr2 = @MyStr2 + '''' + @HourlySalary  + ''', ''' +  @AnnualSalary + ''', ' 
					
				IF @BoClassSequence <> '0'
				BEGIN
					select @MyStr2 = @MyStr2 + '''' + @ClassSequence + ''', ''' +  convert(varchar, getdate(), 126) + ''', ''' + @BoClassSequence + ''')' + Char(13)	
				END
				ELSE 
				BEGIN
					select @MyStr2 = @MyStr2 + '''' + @ClassSequence + ''', ''' +  convert(varchar, getdate(), 126) + ''')' + Char(13)	
				END
												
				select @MyStrAll = @MyStr+@MyStr2+@MyStr3
				--select @MyStrAll
				--print @MyStrAll
					
				BEGIN TRY				
					exec (@MyStrAll);
					Exec spDynamicSqlEmployeeInsert
				END TRY
				BEGIN CATCH					
					DECLARE @Error VARCHAR(MAX)
											
					SET @Error =  CAST(ERROR_NUMBER() as varchar(32)) 
						+ ' ' + CAST(ERROR_SEVERITY() as varchar(32))
						+ ' ' + CAST(ERROR_STATE() as varchar(32))
						+ ' ' + CAST(ERROR_PROCEDURE() as varchar(256))
						+ ' ' + CAST(ERROR_LINE() as varchar(32))
						+ ' ' + CAST(ERROR_MESSAGE() as varchar(4000))
					
					PRINT '		spInsertEmployees failed:' + @Error + CHAR(13)												

					INSERT INTO [dbo].[Logs] ([Date], [Output], [StoredProcedure], Location, DynamicSql)
					SELECT GETDATE(), @Error, '[dbo].[spInsertEmployees]', @LocationName, @MyStrAll
						
	
				END CATCH				
					
			END			
		
		END
			
		FETCH NEXT FROM CurEmployeeUpdate
		INTO
			@LocationId, @EmployeeId, @FirstName, @LastName, 
			@ObjectNumber, @LongFirstName, @LongLastName, @MiddleName,
			@CheckName, @CheckName2, @EffectiveFrom, @DateOfBirth,
			@PartTime, @Salaried, @AddressLine1, @AddressLine2,
			@City, @PostalCode, @LocalNumber, @OtherNumber, 
			@Email, @EmergencyName, @EmergencyNumber, @ContactRelation,
			@Gender, @HireDate, @OriginalHireDate, @SalaryStartDate, 
			@HourlySalary, @AnnualSalary, @ClassSequence,
			@BoClassSequence, @LastUpdatedDate, @RevenueCenterSeq, 
			@TerminationDate, @TerminationNote, @Status, @UpdateRecord,
			@PayrollId, @PasswordId
	END


	CLOSE CurEmployeeUpdate
	DEALLOCATE CurEmployeeUpdate

	


END

