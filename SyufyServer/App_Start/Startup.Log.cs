﻿using System;
using System.IO;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using SyufyServer.Helper;

namespace SyufyServer
{
    public partial class Startup
    {

        public class LogRequestAndResponseHandler : DelegatingHandler
        {
            protected override async Task<HttpResponseMessage> SendAsync(
                HttpRequestMessage request, CancellationToken cancellationToken)
            {

                try
                {
                    string requestBody = await request.Content.ReadAsStringAsync();

                    Log.WriteToLog("REQUEST",request.ToString());

                    Log.WriteToLog("REQUEST BODY", requestBody);

                    // Let other handlers process the request
                    var result = await base.SendAsync(request, cancellationToken);

                    Log.WriteToLog("RESPONSE", result.ToString());

                    return result;

                }
                catch(Exception e)
                {                
                    Log.WriteToLog("EXCEPTION", e.ToString());

                    return null;
                }
            }
        }


    }
}