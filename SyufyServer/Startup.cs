﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;
using System.Web.Configuration;
using System.Web.Security;
using System.Configuration;
using System.IO;
using SyufyServer.Helper;

[assembly: OwinStartup(typeof(SyufyServer.Startup))]

namespace SyufyServer
{
    public partial class Startup
    {

        public void Configuration(IAppBuilder app)
        {
            try
            {
                ConfigureAuth(app);
            }
            catch(Exception e)
            {
                Log.WriteToLog("EXCEPTION", e.ToString());
                Log.SendEmail(String.Format("EXCEPTION: {0}", e.ToString()));
            }
        }
    }
}
