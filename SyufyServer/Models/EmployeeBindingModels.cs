﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SyufyServer.Models
{
    // Models used as parameters to EmployeeController actions.
    /// <summary>
    /// Represents an Micros POS employee and their associated jobs.
    /// </summary>
    public class SetEmployeeBindingModel
    {
        /// <summary>
        /// The Id of the location.  This value must correspond to a record in the HRIS_INIT.dbo.SyufyLocations table used to link location to a Micros POS linked server.
        /// </summary>
        [Display(Name = "Location Id")]
        public int LocationId { get; set; }

        /// <summary>
        /// The obj_num value in the micros.emp_def table in Micros database.  This value will be used to link an employee to the Micros POS.  It must be unique.
        /// </summary>
        [Display(Name = "Employee Id")]
        public int EmployeeId { get; set; }

        /// <summary>
        /// Employee first name.
        /// </summary>
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        /// <summary>
        /// Employee last name.
        /// </summary>
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        /// <summary>
        /// Employee long first name.  Micros field can have up to 25 characters.  Anything over will be truncated by system.  
        /// </summary>
        [Display(Name = "Long First Name")]
        public string LongFirstName { get; set; }

        /// <summary>
        /// Employee long last name.  Micros field can have up to  25 characters.  Anything over will be truncated by system.
        /// </summary>
        [Display(Name = "Lont Last Name")]
        public string LongLastName { get; set; }

        /// <summary>
        /// Employee middle name.
        /// </summary>
        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }

        /// <summary>
        /// Name to be used on Micros POS check.
        /// </summary>
        [Display(Name = "Check Name")]
        public string CheckName { get; set; }

        /// <summary>
        /// Alternate name to be used on Micros POS check.
        /// </summary>
        [Display(Name = "Check Name 2")]
        public string CheckName2 { get; set; }

        /// <summary>
        /// Effective date of the employee record.  The date must be greater than or equal to current date, otherwise system will not insert into POS.
        /// </summary>
        [Display(Name = "Effective From")]
        public DateTime EffectiveFrom { get; set; }

        /// <summary>
        /// Employee date of birth.
        /// </summary>
        [Display(Name = "Date Of Birth")]
        public DateTime DateOfBirth { get; set; }

        /// <summary>
        /// Indicates if Employee is part time.
        /// </summary>
        [Display(Name = "Part Time")]
        public bool PartTime { get; set; }

        /// <summary>
        /// Indicates if Employee is salary.
        /// </summary>
        [Display(Name = "Salaried")]
        public bool Salaried { get; set; }

        /// <summary>
        /// Employee address line one.
        /// </summary>
        [Display(Name = "Address Line 1")]
        public string AddressLine1 { get; set; }

        /// <summary>
        /// Employee address line two.
        /// </summary>
        [Display(Name = "Address Line 2")]
        public string AddressLine2 { get; set; }

        /// <summary>
        /// Employee city.
        /// </summary>
        [Display(Name = "City")]
        public string City { get; set; }

        /// <summary>
        /// Employee postal code.
        /// </summary>
        [Display(Name = "Postal Code")]
        public int PostalCode { get; set; }


        /// <summary>
        /// Employee phone number.
        /// </summary>
        [Display(Name = "Local Number")]
        public string LocalNumber { get; set; }


        /// <summary>
        /// Employee alternate phone number.
        /// </summary>
        [Display(Name = "Other Number")]
        public string OtherNumber { get; set; }

        /// <summary>
        /// Employee email.
        /// </summary>
        [Display(Name = "Email Address")]
        public string Email { get; set; }

        /// <summary>
        /// Employee emergency contact name.
        /// </summary>
        [Display(Name = "Emergency Name")]
        public string EmergencyName { get; set; }

        /// <summary>
        /// Employee emergency contact phone number.
        /// </summary>
        [Display(Name = "Emergency Number")]
        public string EmergencyNumber { get; set; }

        /// <summary>
        /// Employee emergency contact 
        /// </summary>
        [Display(Name = "Contact Relation")]
        public string ContactRelation { get; set; }


        /// <summary>
        /// Employee gender.  Micros accepts Male or Female.
        /// </summary>
        [Display(Name = "Gender")]
        public string Gender { get; set; }

        /// <summary>
        /// Employee hire date.
        /// </summary>
        [Display(Name = "Hire Date")]
        public DateTime HireDate { get; set; }

        /// <summary>
        /// Employee original hire date.
        /// </summary>
        [Display(Name = "Original Hire Date")]
        public DateTime OriginalHireDate { get; set; }

        /// <summary>
        /// Employe salary start time.
        /// </summary>
        [Display(Name = "Salary Start Date")]
        public DateTime SalaryStartDate { get; set; }

        /// <summary>
        /// Employee hourly salary.
        /// </summary>
        [Display(Name = "Hourly Salary")]
        public decimal HourlySalary { get; set; }

        /// <summary>
        /// Employee annual salary.
        /// </summary>
        [Display(Name = "AnnualSalary")]
        public decimal AnnualSalary { get; set; }

        /// <summary>
        /// Employee termination date.
        /// </summary>
        [Display(Name = "Termination Date")]
        public DateTime? TerminationDate { get; set; }

        /// <summary>
        /// Employee termination reason/note.
        /// </summary>
        [Display(Name = "Termination Note")]
        public string TerminationNote { get; set; }

        /// <summary>
        /// Employee POS login id.
        /// </summary>
        [Display(Name = "PasswordId")]
        public int PasswordId { get; set; }

        /// <summary>
        /// This value corresponds to a record in the HRIS_INIT. dbo. DefaultEmployeeTemplate  table.  This is responsible for setting the default values such as manager/security level and current revenue center.  Set to 1.
        /// </summary>
        [Display(Name = "Default Template Id")]
        public int DefaultTemplateId { get; set; }

        /// <summary>
        /// Employee payroll id.
        /// </summary>
        [Display(Name = "Payroll Id")]
        public int PayrollId { get; set; }

        /// <summary>
        /// Represents a collection of Micros POS jobs.
        /// </summary>
        public SetEmployeeJobBindingModel[] Jobs { get; set; }
    }

    /// <summary>
    /// Represents a Micros POS job.
    /// </summary>
    public class SetEmployeeJobBindingModel
    {

        /// <summary>
        /// The obj_num value in the micros.job_def table in Micros database.  This value will be used to link a job to the Micros POS.  It must be unique.
        /// </summary>
        [Display(Name ="Job Id")]
        public int JobId { get; set; }

        /// <summary>
        /// Job name.
        /// </summary>
        [Display(Name="Job Name")]
        public string Name { get; set; }

        /// <summary>
        /// Job rate.
        /// </summary>
        [Display(Name ="Override Regular Pay Rate")]
        public decimal OverrideRegPayRate { get; set; }

        /// <summary>
        /// Job effective date.
        /// </summary>
        [Display(Name ="Rate Effective Datetime")]
        public DateTime RateEffectiveDatetime { get; set; }

        /// <summary>
        /// Value indicates whether job is primary.
        /// </summary>
        [Display(Name ="Primary Job")]
        public bool PrimaryJob { get; set; }

        /// <summary>
        /// This value corresponds to a record in the HRIS_INIT. dbo. DefaultJobTemplate  table.  This is responsible for setting the default values.  Set to 1.
        /// </summary>
        [Display(Name = "Default Template Id")]
        public int DefaultTemplateId { get; set; }

    }

}