﻿using System;

namespace SyufyServer.Data
{
    public class Jobs
    {
        public int Id { get; set; }

        public int EmployeeId { get; set; }

        public int JobId { get; set; }

        public string Name { get; set; }

        public decimal OverrideRegPayRate { get; set; }

        public DateTime RateEffectiveDatetime { get; set; }

        public bool PrimaryJob { get; set; }

        public int DefaultTemplateId { get; set; }

    }
}