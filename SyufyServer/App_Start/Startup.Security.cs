﻿using System.Configuration;
using System.IO;
using System.Web.Configuration;

namespace SyufyServer
{
    public partial class Startup
    {
        public void EncryptConnString()
        {

            Configuration config = OpenConfigFile("C:\\Git\\Work\\SyufyServer\\SyufyServer\\Web.config");
            ConfigurationSection section = config.GetSection("connectionStrings");
            if (!section.SectionInformation.IsProtected)
            {
                section.SectionInformation.ProtectSection("RsaProtectedConfigurationProvider");
                config.Save();
            }
        }

        public void DecryptConnString()
        {
            Configuration config = WebConfigurationManager.OpenWebConfiguration("C:\\Git\\Work\\SyufyServer\\SyufyServer\\Web.config");
            ConfigurationSection section = config.GetSection("connectionStrings");
            if (section.SectionInformation.IsProtected)
            {
                section.SectionInformation.UnprotectSection();
                config.Save();
            }
        }

        public static Configuration OpenConfigFile(string configPath)
        {
            var configFile = new FileInfo(configPath);
            var vdm = new VirtualDirectoryMapping(configFile.DirectoryName, true, configFile.Name);
            var wcfm = new WebConfigurationFileMap();
            wcfm.VirtualDirectories.Add("/", vdm);
            return WebConfigurationManager.OpenMappedWebConfiguration(wcfm, "/");
        }
    }
}