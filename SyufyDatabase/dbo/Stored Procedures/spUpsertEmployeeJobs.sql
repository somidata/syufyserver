﻿







-- =============================================
-- Author:		SomiData
-- Create date: 4/7/2016
-- Description:	Updates and inserts Syufy employee jobs.
-- =============================================
CREATE PROCEDURE [dbo].[spUpsertEmployeeJobs]
	@LocationName varchar(100) = NULL, @LocationNumber varchar(50) = NULL, 
	@LinkedServer  varchar(100) = NULL, @IncrementalUpdate int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
		EXEC spDownloadMicrosEmployees @LocationName, @LocationNumber, @LinkedServer
		
		EXEC spDownloadMicrosJobDefinition @LocationName, @LocationNumber, @LinkedServer
		
		EXEC spUpdateEmployeeJobs @LocationName, @LocationNumber, @LinkedServer, @IncrementalUpdate
	
		EXEC spInsertEmployeesJobs @LocationName, @LocationNumber, @LinkedServer, @IncrementalUpdate


END












