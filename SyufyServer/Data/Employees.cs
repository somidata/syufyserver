﻿using System;

namespace SyufyServer.Data
{
    public class Employees
    {
        public int Id { get; set; }

        public int LocationId { get; set; }

        public int EmployeeId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string LongFirstName { get; set; }

        public string LongLastName { get; set; }

        public string MiddleName { get; set; }

        public string CheckName { get; set; }

        public string CheckName2 { get; set; }

        public DateTime EffectiveFrom { get; set; }

        public DateTime DateOfBirth { get; set; }

        public bool PartTime { get; set; }

        public bool Salaried { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string City { get; set; }

        public int PostalCode { get; set; }

        public string LocalNumber { get; set; }

        public string OtherNumber { get; set; }

        public string Email { get; set; }

        public string EmergencyName { get; set; }

        public string EmergencyNumber { get; set; }

        public string ContactRelation { get; set; }

        public string Gender { get; set; }

        public DateTime HireDate { get; set; }

        public DateTime OriginalHireDate { get; set; }

        public DateTime SalaryStartDate { get; set; }

        public decimal HourlySalary { get; set; }

        public decimal AnnualSalary { get; set; }

        public DateTime? TerminationDate { get; set; }

        public string TerminationNote { get; set; }

        public int PasswordId { get; set; }

        public int DefaultTemplateId { get; set; }

        public int PayrollId { get; set; }

    }
}