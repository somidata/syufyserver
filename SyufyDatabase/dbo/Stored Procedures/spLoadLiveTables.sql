﻿-- =============================================
-- Author:		SomiData
-- Create date: 4/7/2016
-- Description:	Loads live tables.
-- =============================================
CREATE PROCEDURE [dbo].[spLoadLiveTables]


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [dbo].[EmployeesLive]
		(
			 [LocationId]
			,[EmployeeId]
			,[FirstName]
			,[LastName]
			,[LongFirstName]
			,[LongLastName]
			,[MiddleName]
			,[CheckName]
			,[CheckName2]
			,[EffectiveFrom]
			,[DateOfBirth]
			,[PartTime]
			,[Salaried]
			,[AddressLine1]
			,[AddressLine2]
			,[City]
			,[PostalCode]
			,[LocalNumber]
			,[OtherNumber]
			,[Email]
			,[EmergencyName]
			,[EmergencyNumber]
			,[ContactRelation]
			,[Gender]
			,[HireDate]
			,[OriginalHireDate]
			,[SalaryStartDate]
			,[HourlySalary]
			,[AnnualSalary]
			,[LastUpdatedDate]
			,[TerminationDate]
			,[TerminationNote]
			,[Status] 
			,[UpdateRecord]
			,[PayrollId]
			,[DefaultTemplateId]
			,[PasswordId]
		)

		SELECT
			 [LocationId]
			,[EmployeeId]
			,REPLACE([FirstName],'''','''''')
			,REPLACE([LastName],'''','''''')
			,REPLACE([LongFirstName],'''','''''')
			,REPLACE([LongLastName],'''','''''')
			,REPLACE([MiddleName],'''','''''')
			,REPLACE([CheckName],'''','''''')
			,REPLACE([CheckName2],'''','''''')
			,[EffectiveFrom]
			,[DateOfBirth]
			,[PartTime]
			,[Salaried]
			,REPLACE([AddressLine1],'''','''''')
			,REPLACE([AddressLine2],'''','''''')
			,REPLACE([City],'''','''''')
			,REPLACE([PostalCode],'''','''''')
			,REPLACE([LocalNumber],'''','''''')
			,REPLACE([OtherNumber],'''','''''')
			,REPLACE([Email],'''','''''')
			,REPLACE([EmergencyName],'''','''''')
			,REPLACE([EmergencyNumber],'''','''''')
			,REPLACE([ContactRelation],'''','''''')
			,[Gender]
			,[HireDate]
			,[OriginalHireDate]
			,[SalaryStartDate]
			,[HourlySalary]
			,[AnnualSalary]
			,[LastUpdatedDate]
			,[TerminationDate]
			,REPLACE([TerminationNote],'''','''''')
			,[Status]
			,1 
			,[PayrollId]
			,[DefaultTemplateId]
			,[PasswordId]
		FROM [dbo].[Employees]
		ORDER BY LastUpdatedDate

		INSERT INTO [dbo].[JobsLive]
		(
			 [EmployeeId]
			,[JobId]
			,[Name]
			,[OverrideRegPayRate]
			,[RateEffectiveDateTime]
			,[LastUpdatedDate]
			,[PrimaryJob]
			,[DefaultTemplateId]
		)
		
		SELECT 
			 [EmployeeId]
			,[JobId]
			,[Name]
			,[OverrideRegPayRate]
			,[RateEffectiveDateTime]
			,[LastUpdatedDate]
			,[PrimaryJob]
			,[DefaultTemplateId]
		FROM [dbo].[Jobs]
		ORDER BY LastUpdatedDate


	END TRY
	BEGIN CATCH

		DECLARE @fdError VARCHAR(MAX)
										
		SET @fdError =  CAST(ERROR_NUMBER() as varchar(32)) 
			+ ' ' + CAST(ERROR_SEVERITY() as varchar(32))
			+ ' ' + CAST(ERROR_STATE() as varchar(32))
			+ ' ' + CAST(ERROR_PROCEDURE() as varchar(256))
			+ ' ' + CAST(ERROR_LINE() as varchar(32))
			+ ' ' + CAST(ERROR_MESSAGE() as varchar(4000))
					
		INSERT INTO [dbo].[Logs] ([Date], [Output], [StoredProcedure])
		SELECT GETDATE(), @fdError, '[dbo].[spLoadLiveTables]'
					
		PRINT '		' + @fdError + CHAR(13)

	
	END CATCH		

END
