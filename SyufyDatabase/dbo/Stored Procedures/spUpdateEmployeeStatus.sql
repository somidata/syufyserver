﻿

-- =============================================
-- Author:		SomiData
-- Create date: 4/8/2016
-- Description:	Updates Micros employee status data..
-- =============================================
CREATE PROCEDURE [dbo].[spUpdateEmployeeStatus]
	@LocationName varchar(100) = NULL, @LocationNumber varchar(50) = NULL, 
	@LinkedServer  varchar(100) = NULL, @IncrementalUpdate int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
	
	--PRINT '				Update Employee Status ' + CHAR(13)
	
	DECLARE @MyStr varchar(Max), @MyStr2 varchar(Max), @MyStr3 varchar(Max), @MyStrAll varchar(Max),
			@LocationId varchar(100), @EmployeeId varchar(100), @FirstName varchar(100), @LastName varchar(100), 
			@ObjectNumber varchar(100), @LongFirstName varchar(100), @LongLastName varchar(100), @MiddleName varchar(100),
			@CheckName varchar(100), @CheckName2 varchar(100), @EffectiveFrom varchar(100), @DateOfBirth varchar(100),
			@PartTime varchar(100), @Salaried varchar(100), @AddressLine1 varchar(100), @AddressLine2 varchar(100),
			@City varchar(100), @PostalCode varchar(100), @LocalNumber varchar(100), @OtherNumber varchar(100), 
			@Email varchar(100), @EmergencyName varchar(100), @EmergencyNumber varchar(100), @ContactRelation varchar(100),
			@Gender varchar(100), @HireDate varchar(100), @OriginalHireDate varchar(100), @SalaryStartDate varchar(100), 
			@HourlySalary varchar(100), @AnnualSalary varchar(100), @ClassSequence varchar(100),
			@BoClassSequence varchar(100), @LastUpdatedDate varchar(100), @RevenueCenterSeq varchar(100),
			@TerminationDate varchar(100), @TerminationNote varchar(100), @Status varchar(100), 
			@UpdateRecord varchar(100), @EmployeeExists varchar(100), @ReturnValue varchar(100),
			@EmployeeSequence varchar(100)
			
	DECLARE CurEmployeeStatusUpdate CURSOR FOR

	SELECT
		 [LocationId],[EmployeeId],[FirstName],[LastName],[ObjectNumber],[LongFirstName],[LongLastName],[MiddleName]
		,[CheckName],[CheckName2],[EffectiveFrom],[DateOfBirth],[PartTime],[Salaried],[AddressLine1],[AddressLine2]
		,[City],[PostalCode],[LocalNumber],[OtherNumber],[Email],[EmergencyName],[EmergencyNumber],[ContactRelation]
		,[Gender],[HireDate],[OriginalHireDate],[SalaryStartDate],[HourlySalary],[AnnualSalary],[ClassSequence]
		,[BoClassSequence], [LastUpdatedDate], [RevenueCenterSeq], [TerminationNote], [TerminationDate]
		,[Status], [UpdateRecord], [EmployeeSequence]
	FROM [dbo].[EmployeesLive]
	WHERE LocationId = @LocationNumber
	
	OPEN CurEmployeeStatusUpdate
	FETCH NEXT FROM CurEmployeeStatusUpdate
	INTO  
		@LocationId, @EmployeeId, @FirstName, @LastName, 
		@ObjectNumber, @LongFirstName, @LongLastName, @MiddleName,
		@CheckName, @CheckName2, @EffectiveFrom, @DateOfBirth,
		@PartTime, @Salaried, @AddressLine1, @AddressLine2,
		@City, @PostalCode, @LocalNumber, @OtherNumber, 
		@Email, @EmergencyName, @EmergencyNumber, @ContactRelation,
		@Gender, @HireDate, @OriginalHireDate, @SalaryStartDate, 
		@HourlySalary, @AnnualSalary, @ClassSequence,
		@BoClassSequence, @LastUpdatedDate, @RevenueCenterSeq, 
		@TerminationDate, @TerminationNote, @Status, @UpdateRecord,
		@EmployeeSequence
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @UpdateRecord = 1
		BEGIN
		 
			IF @RevenueCenterSeq <> '0'
			BEGIN
			
				--PRINT '				Updating employeeID ' + @employeeID + ' job_seq ' + @job_seq + ' ...' + CHAR(13)
				
				select @MyStr = 'ALTER PROCEDURE [dbo].[spDynamicSqlEmployeeStatusUpdate]' + Char(13)
				select @MyStr = @MyStr + 'AS '  + Char(13)
				select @MyStr = @MyStr + 'BEGIN'  + Char(13)
				select @MyStr3 = ' END'

				select @MyStr2 = '	UPDATE OPENQUERY(' + @LinkedServer + ',''SELECT' + Char(13)
				select @MyStr2 = @MyStr2 + 'E.current_rvc_seq, E.last_updated_date ' + Char(13)
				select @MyStr2 = @MyStr2 + 'FROM micros.emp_status E WHERE E.emp_seq = ' + @EmployeeSequence + ''')' + Char(13)
				select @MyStr2 = @MyStr2 + 'SET current_rvc_seq = ' + @RevenueCenterSeq + ',' + Char(13)
				select @MyStr2 = @MyStr2 + 'last_updated_date = ''' +  convert(varchar, getdate(), 126) + '''' + Char(13)
				select @MyStrAll = @MyStr+@MyStr2+@MyStr3
				--print @MyStrAll

				BEGIN TRY				
					exec (@MyStrAll);
					Exec spDynamicSqlEmployeeStatusUpdate
				END TRY
				BEGIN CATCH					
					DECLARE @Error VARCHAR(MAX)
										
					SET @Error =  CAST(ERROR_NUMBER() as varchar(32)) 
						+ ' ' + CAST(ERROR_SEVERITY() as varchar(32))
						+ ' ' + CAST(ERROR_STATE() as varchar(32))
						+ ' ' + CAST(ERROR_PROCEDURE() as varchar(256))
						+ ' ' + CAST(ERROR_LINE() as varchar(32))
						+ ' ' + CAST(ERROR_MESSAGE() as varchar(4000))
					
					PRINT '		spUpdateEmployeeStatus failed:' + @Error + CHAR(13)

					INSERT INTO [dbo].[Logs] ([Date], [Output], [StoredProcedure], [Location], [DynamicSql])
					SELECT GETDATE(), @Error, '[dbo].[spUpdateEmployeeStatus]', @LocationName, @MyStrAll					
												
				END CATCH										 		
			END
			
		END
			
		FETCH NEXT FROM CurEmployeeStatusUpdate
		INTO  
			@LocationId, @EmployeeId, @FirstName, @LastName, 
			@ObjectNumber, @LongFirstName, @LongLastName, @MiddleName,
			@CheckName, @CheckName2, @EffectiveFrom, @DateOfBirth,
			@PartTime, @Salaried, @AddressLine1, @AddressLine2,
			@City, @PostalCode, @LocalNumber, @OtherNumber, 
			@Email, @EmergencyName, @EmergencyNumber, @ContactRelation,
			@Gender, @HireDate, @OriginalHireDate, @SalaryStartDate, 
			@HourlySalary, @AnnualSalary, @ClassSequence,
			@BoClassSequence, @LastUpdatedDate, @RevenueCenterSeq, 
			@TerminationDate, @TerminationNote, @Status, @UpdateRecord,
			@EmployeeSequence
	END


	CLOSE CurEmployeeStatusUpdate
	DEALLOCATE CurEmployeeStatusUpdate

	


END














