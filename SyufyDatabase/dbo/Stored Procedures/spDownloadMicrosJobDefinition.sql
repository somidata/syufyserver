﻿





-- =============================================
-- Author:		SomiData
-- Create date: 4/8/2016
-- Description:	Retrieves job primary key from Micros and update in local table.
-- =============================================
CREATE PROCEDURE [dbo].[spDownloadMicrosJobDefinition]
	@LocationName varchar(100) = NULL, @LocationNumber varchar(50) = NULL, 
	@LinkedServer  varchar(100) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @MyStr varchar(Max), @MyStr2 varchar(Max), @MyStr3 varchar(Max), @MyStrAll varchar(Max), @MyStr5 varchar(Max)

	TRUNCATE TABLE dbo.MicrosJobsDefinition						

	select @MyStr = 'ALTER PROCEDURE [dbo].[spDynamicSqlDownloadMicrosJobsDefinition]' + Char(13)
	select @MyStr = @MyStr + 'AS '  + Char(13)
	select @MyStr = @MyStr + 'BEGIN'  + Char(13)
	select @MyStr3 = ' END'

	select @MyStr2 = '	INSERT INTO dbo.MicrosJobsDefinition (JobSequence, ObjectNumber, JobName, LocationNumber) ' + Char(13)
	select @MyStr2 = @MyStr2 + 'SELECT job_seq, obj_num, [name], ''' + @LocationNumber + '''' + Char(13)
	select @MyStr2 = @MyStr2 + 'FROM OPENQUERY(' + @LinkedServer + ',''SELECT' + Char(13)	
	select @MyStr2 = @MyStr2 + 'job_seq, obj_num, [name] ' + Char(13)
	select @MyStr2 = @MyStr2 + 'FROM micros.job_def '')' + Char(13)
	select @MyStrAll = @MyStr+@MyStr2+@MyStr3

	BEGIN TRY
	
		exec (@MyStrAll);
		--print @MyStrAll
		Exec spDynamicSqlDownloadMicrosJobsDefinition
		
		UPDATE J 
		SET J.JobSequence = M.JobSequence
		FROM [dbo].[JobsLive] J
		INNER JOIN [dbo].[EmployeesLive] E ON J.EmployeeId = E.EmployeeId
		INNER JOIN [dbo].[MicrosJobsDefinition] M ON 
		CAST(J.JobId AS INT) = CAST(M.ObjectNumber AS INT) AND
		E.LocationId = M.LocationNumber
		WHERE E.LocationId = @LocationNumber
	
	END TRY
	BEGIN CATCH					
		
		DECLARE @Error VARCHAR(MAX)
							
		SET @Error =  CAST(ERROR_NUMBER() as varchar(32)) 
			+ ' ' + CAST(ERROR_SEVERITY() as varchar(32))
			+ ' ' + CAST(ERROR_STATE() as varchar(32))
			+ ' ' + CAST(ERROR_PROCEDURE() as varchar(256))
			+ ' ' + CAST(ERROR_LINE() as varchar(32))
			+ ' ' + CAST(ERROR_MESSAGE() as varchar(4000))
		
		INSERT INTO [dbo].[Logs] ([Date], [Output], [StoredProcedure], [Location])
		SELECT GETDATE(), @Error, '[dbo].[spDownloadMicrosJobDefinition]', @LocationName
		
		PRINT '		spDownloadMicrosJobDefinition failed:' + @Error + CHAR(13)

	END CATCH		

END













