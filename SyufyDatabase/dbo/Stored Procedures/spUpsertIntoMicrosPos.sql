﻿



-- =============================================
-- Author:		SomiData
-- Create date: 4/7/2016
-- Description:	Upserts Syufy Employee data into Micros POS per location.
-- =============================================
CREATE PROCEDURE [dbo].[spUpsertIntoMicrosPos]
@IncrementalUpdate int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @LocationName varchar(100), @LocationNumber varchar(50), @Active bit, 
	@LinkedServer  varchar(100), @srvr nvarchar(128), @ReturnValueOutput int, @ReturnValue int	
	

    -- Insert statements for procedure here
	DECLARE CurSyufyLocations CURSOR FOR
	
	SELECT
	   [LocationName]
	  ,[LocationNumber]
	  ,[Active]
	  ,[LinkedServer]
	FROM [dbo].[SyufyLocations]
	WHERE Active = 1

	
	OPEN CurSyufyLocations
	FETCH NEXT FROM CurSyufyLocations
	INTO  @LocationName, @LocationNumber, @Active, @LinkedServer 
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
	
		EXEC @ReturnValue = spCheckLinkServerConnectivity @LocationName = @LocationName, @LinkedServer = @LinkedServer, @ReturnValueOutput = @ReturnValueOutput output

		IF @ReturnValueOutput = 0
		BEGIN
				
			PRINT '			Upserting ' + @LocationName + ' ' + @LocationNumber + ' ' + @LinkedServer + ' ...' + CHAR(13)

			EXEC spDownloadMicrosSecurityLevelDefinition @LocationName, @LocationNumber, @LinkedServer

			EXEC spDownloadMicrosRevenueCenterDefinition @LocationName, @LocationNumber, @LinkedServer

			EXEC spDownloadMicrosManagerLevelDefinition @LocationName, @LocationNumber, @LinkedServer
			 
			EXEC spUpsertEmployees @LocationName, @LocationNumber, @LinkedServer, @IncrementalUpdate
					
			EXEC spUpsertEmployeeJobs @locationName, @LocationNumber, @LinkedServer, @IncrementalUpdate
			
			EXEC spUpdateEmployeeStatus @locationName, @LocationNumber, @LinkedServer, @IncrementalUpdate
		END
		ELSE
		BEGIN
			PRINT '			Unable to reach ' + @LocationName + ' ' + @LocationNumber + ' ' + @LinkedServer + ' ...' + CHAR(13)
		END
			
		FETCH NEXT FROM CurSyufyLocations
		INTO  @LocationName, @LocationNumber, @Active, @LinkedServer 
	END


	CLOSE CurSyufyLocations
	DEALLOCATE CurSyufyLocations
	


END








