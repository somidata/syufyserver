﻿

CREATE PROCEDURE [dbo].[spCheckLinkServerConnectivity]
@LocationName varchar(100) = NULL, @LinkedServer  varchar(100) = NULL, 
@ReturnValueOutput int out
AS 
BEGIN

	DECLARE @LinkedServerToTest nvarchar(128)	

	SET @LinkedServerToTest = @LinkedServer;
	SET @ReturnValueOutput = 0
	BEGIN TRY
		EXEC @ReturnValueOutput = [sys].[sp_testlinkedserver] @LinkedServerToTest;
	END TRY
	BEGIN CATCH
		SET @ReturnValueOutput = sign(@@error);
		DECLARE @Error VARCHAR(MAX)
							
		SET @Error =  CAST(ERROR_NUMBER() as varchar(32)) 
			+ ' ' + CAST(ERROR_SEVERITY() as varchar(32))
			+ ' ' + CAST(ERROR_STATE() as varchar(32))
			+ ' ' + CAST(ERROR_PROCEDURE() as varchar(256))
			+ ' ' + CAST(ERROR_LINE() as varchar(32))
			+ ' ' + CAST(ERROR_MESSAGE() as varchar(4000))
		
		INSERT INTO [dbo].[Logs] ([Date], [Output], [StoredProcedure], [Location])
		SELECT GETDATE(), @Error, '[dbo].[spCheckLinkServerConnectivity]', @LocationName
		
		PRINT '		' + @Error + CHAR(13)
	END CATCH;
		
 END



