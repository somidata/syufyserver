﻿CREATE TABLE [dbo].[Logs] (
    [Id]              INT           IDENTITY (1, 1) NOT NULL,
    [Date]            DATETIME      NULL,
    [Output]          VARCHAR (MAX) NULL,
    [Location]        VARCHAR (50)  NULL,
    [StoredProcedure] VARCHAR (50)  NULL,
    [DynamicSql]      VARCHAR (MAX) NULL,
    CONSTRAINT [PK_tblLog] PRIMARY KEY CLUSTERED ([Id] ASC)
);

