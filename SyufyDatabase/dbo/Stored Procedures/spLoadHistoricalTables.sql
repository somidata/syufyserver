﻿-- =============================================
-- Author:		SomiData
-- Create date: 4/8/2016
-- Description:	Loads historical tables.
-- =============================================
CREATE PROCEDURE [dbo].[spLoadHistoricalTables]


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [dbo].[EmployeesHistorical]
		(
			 [LocationId]
			,[EmployeeId]
			,[FirstName]
			,[LastName]
			,[LongFirstName]
			,[LongLastName]
			,[MiddleName]
			,[CheckName]
			,[CheckName2]
			,[EffectiveFrom]
			,[DateOfBirth]
			,[PartTime]
			,[Salaried]
			,[AddressLine1]
			,[AddressLine2]
			,[City]
			,[PostalCode]
			,[LocalNumber]
			,[OtherNumber]
			,[Email]
			,[EmergencyName]
			,[EmergencyNumber]
			,[ContactRelation]
			,[Gender]
			,[HireDate]
			,[OriginalHireDate]
			,[SalaryStartDate]
			,[HourlySalary]
			,[AnnualSalary]
			,[LastUpdatedDate]
			,[TerminationDate]
			,[TerminationNote]
			,[Status]
			,[PayrollId]
			,[DefaultTemplateId]
			,[PasswordId] 
		)

		SELECT
			 [LocationId]
			,[EmployeeId]
			,[FirstName]
			,[LastName]
			,[LongFirstName]
			,[LongLastName]
			,[MiddleName]
			,[CheckName]
			,[CheckName2]
			,[EffectiveFrom]
			,[DateOfBirth]
			,[PartTime]
			,[Salaried]
			,[AddressLine1]
			,[AddressLine2]
			,[City]
			,[PostalCode]
			,[LocalNumber]
			,[OtherNumber]
			,[Email]
			,[EmergencyName]
			,[EmergencyNumber]
			,[ContactRelation]
			,[Gender]
			,[HireDate]
			,[OriginalHireDate]
			,[SalaryStartDate]
			,[HourlySalary]
			,[AnnualSalary]
			,[LastUpdatedDate]
			,[TerminationDate]
			,[TerminationNote]
			,[Status] 
			,[PayrollId]
			,[DefaultTemplateId]
			,[PasswordId]
		FROM [dbo].[Employees]

		INSERT INTO [dbo].[JobsHistorical]
		(
			 [EmployeeId]
			,[JobId]
			,[Name]
			,[OverrideRegPayRate]
			,[RateEffectiveDateTime]
			,[LastUpdatedDate]
			,[PrimaryJob]
			,[DefaultTemplateId]
		)
		
		SELECT 
			 [EmployeeId]
			,[JobId]
			,[Name]
			,[OverrideRegPayRate]
			,[RateEffectiveDateTime]
			,[LastUpdatedDate]
			,[PrimaryJob]
			,[DefaultTemplateId]
		FROM [dbo].[Jobs]


	END TRY
	BEGIN CATCH

		DECLARE @fdError VARCHAR(MAX)
										
		SET @fdError =  CAST(ERROR_NUMBER() as varchar(32)) 
			+ ' ' + CAST(ERROR_SEVERITY() as varchar(32))
			+ ' ' + CAST(ERROR_STATE() as varchar(32))
			+ ' ' + CAST(ERROR_PROCEDURE() as varchar(256))
			+ ' ' + CAST(ERROR_LINE() as varchar(32))
			+ ' ' + CAST(ERROR_MESSAGE() as varchar(4000))
					
		INSERT INTO [dbo].[Logs] ([Date], [Output], [StoredProcedure])
		SELECT GETDATE(), @fdError, '[dbo].[spLoadHistoricalTables]'
					
		PRINT '		' + @fdError + CHAR(13)

	
	END CATCH		

END



