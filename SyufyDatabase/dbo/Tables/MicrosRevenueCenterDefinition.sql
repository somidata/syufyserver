﻿CREATE TABLE [dbo].[MicrosRevenueCenterDefinition] (
    [RevenueCenterSequence] VARCHAR (100) NULL,
    [ObjectNumber]          VARCHAR (100) NULL,
    [Name]                  VARCHAR (100) NULL,
    [LocationNumber]        VARCHAR (100) NULL
);

