﻿

-- =============================================
-- Author:		SomiData
-- Create date: 4/7/2016
-- Description:	Empties staging tables.
-- =============================================
CREATE PROCEDURE [dbo].[spEmptyStagingTables]


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	TRUNCATE TABLE [dbo].[Employees]
	TRUNCATE TABLE [dbo].[Jobs]

END




