﻿


-- =============================================
-- Author:		SomiData
-- Create date: 4/8/2016
-- Description:	Retrieves revenue center level definition from Micros and update in local table.
-- =============================================
CREATE PROCEDURE [dbo].[spDownloadMicrosRevenueCenterDefinition]
	@LocationName varchar(100) = NULL, @LocationNumber varchar(50) = NULL, 
	@LinkedServer  varchar(100) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @MyStr varchar(Max), @MyStr2 varchar(Max), @MyStr3 varchar(Max), @MyStrAll varchar(Max), @MyStr5 varchar(Max)

	TRUNCATE TABLE dbo.MicrosRevenueCenterDefinition						

	select @myStr = 'ALTER PROCEDURE [dbo].[spDynamicSqlDownloadMicrosRevenueCenterDefinition]' + Char(13)
	select @MyStr = @MyStr + 'AS '  + Char(13)
	select @MyStr = @MyStr + 'BEGIN'  + Char(13)
	select @MyStr3 = ' END'

	select @MyStr2 = 'INSERT INTO dbo.MicrosRevenueCenterDefinition (RevenueCenterSequence, ObjectNumber, Name, LocationNumber) ' + Char(13)
	select @MyStr2 = @MyStr2 + 'SELECT rvc_seq, obj_num, name, ''' + @LocationNumber + '''' + Char(13)
	select @MyStr2 = @MyStr2 + 'FROM OPENQUERY(' + @LinkedServer + ',''SELECT' + Char(13)	
	select @MyStr2 = @MyStr2 + 'rvc_seq, obj_num, name ' + Char(13)
	select @MyStr2 = @MyStr2 + 'FROM micros.rvc_def '')' + Char(13)
	select @MyStrAll = @MyStr+@MyStr2+@MyStr3

	BEGIN TRY
	
		exec (@MyStrAll);
		--print @MyStrAll
		Exec spDynamicSqlDownloadMicrosRevenueCenterDefinition	
		
		UPDATE E
		SET E.RevenueCenterSeq = M.RevenueCenterSequence
		FROM [dbo].[EmployeesLive] E
		INNER JOIN [dbo].[MicrosRevenueCenterDefinition] M ON E.LocationId = M.LocationNumber
		INNER JOIN [dbo].[DefaultEmployeeTemplate] T ON E.LocationId = T.LocationId
		AND E.DefaultTemplateId = T.DefaultTemplateId
		AND T.RevenueCenterSeqName = M.Name
		AND E.LocationId = @LocationNumber
	
	END TRY
	BEGIN CATCH					
		
		DECLARE @Error VARCHAR(MAX)
							
		SET @Error =  CAST(ERROR_NUMBER() as varchar(32)) 
			+ ' ' + CAST(ERROR_SEVERITY() as varchar(32))
			+ ' ' + CAST(ERROR_STATE() as varchar(32))
			+ ' ' + CAST(ERROR_PROCEDURE() as varchar(256))
			+ ' ' + CAST(ERROR_LINE() as varchar(32))
			+ ' ' + CAST(ERROR_MESSAGE() as varchar(4000))
		
		INSERT INTO [dbo].[Logs] ([Date], [Output], [StoredProcedure], [Location])
		SELECT GETDATE(), @Error, '[dbo].[spDownloadMicrosRevenueCenterDefinition]', @LocationName
		
		PRINT '		spDownloadMicrosRevenueCenterDefinition failed:' + @Error + CHAR(13)

	END CATCH		

END















