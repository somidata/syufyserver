﻿CREATE TABLE [dbo].[MicrosJobsDefinition] (
    [JobSequence]    VARCHAR (250) NULL,
    [ObjectNumber]   VARCHAR (250) NULL,
    [LocationNumber] VARCHAR (50)  NULL,
    [JobName]        VARCHAR (250) NULL
);

