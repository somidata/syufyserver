﻿
-- =============================================
-- Author:		SomiData
-- Create date: 4/7/2016
-- Description:	Updates Micros employee data.
-- =============================================
CREATE PROCEDURE [dbo].[spUpdateEmployees]
	@LocationName varchar(100) = NULL, @LocationNumber varchar(50) = NULL, 
	@LinkedServer  varchar(100) = NULL, @IncrementalUpdate int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @MyStr varchar(Max), @MyStr2 varchar(Max), @MyStr3 varchar(Max), @MyStrAll varchar(Max),
			@LocationId varchar(100), @EmployeeId varchar(100), @FirstName varchar(100), @LastName varchar(100), 
			@ObjectNumber varchar(100), @LongFirstName varchar(100), @LongLastName varchar(100), @MiddleName varchar(100),
			@CheckName varchar(100), @CheckName2 varchar(100), @EffectiveFrom varchar(100), @DateOfBirth varchar(100),
			@PartTime varchar(100), @Salaried varchar(100), @AddressLine1 varchar(100), @AddressLine2 varchar(100),
			@City varchar(100), @PostalCode varchar(100), @LocalNumber varchar(100), @OtherNumber varchar(100), 
			@Email varchar(100), @EmergencyName varchar(100), @EmergencyNumber varchar(100), @ContactRelation varchar(100),
			@Gender varchar(100), @HireDate varchar(100), @OriginalHireDate varchar(100), @SalaryStartDate varchar(100), 
			@HourlySalary varchar(100), @AnnualSalary varchar(100), @ClassSequence varchar(100),
			@BoClassSequence varchar(100), @LastUpdatedDate varchar(100), @RevenueCenterSeq varchar(100),
			@TerminationDate varchar(100), @TerminationNote varchar(100), @Status varchar(100), 
			@UpdateRecord varchar(100), @IsTerminated varchar(100), @PayrollId varchar(100),
			@PasswordId varchar(100)
			
	DECLARE CurEmployeeUpdate CURSOR FOR

	SELECT
		 [LocationId],[EmployeeId],[FirstName],[LastName],[ObjectNumber],[LongFirstName],[LongLastName],[MiddleName]
		,[CheckName],[CheckName2],[EffectiveFrom],[DateOfBirth],[PartTime],[Salaried],[AddressLine1],[AddressLine2]
		,[City],[PostalCode],[LocalNumber],[OtherNumber],[Email],[EmergencyName],[EmergencyNumber],[ContactRelation]
		,[Gender],[HireDate],[OriginalHireDate],[SalaryStartDate],[HourlySalary],[AnnualSalary],[ClassSequence]
		,[BoClassSequence], [LastUpdatedDate], [RevenueCenterSeq], [TerminationDate], [TerminationNote]
		,[Status], [UpdateRecord], [PayrollId], [PasswordId]
	FROM [dbo].[EmployeesLive]
	WHERE LocationId = @LocationNumber
	
	OPEN CurEmployeeUpdate
	FETCH NEXT FROM CurEmployeeUpdate
	INTO  
		@LocationId, @EmployeeId, @FirstName, @LastName, 
		@ObjectNumber, @LongFirstName, @LongLastName, @MiddleName,
		@CheckName, @CheckName2, @EffectiveFrom, @DateOfBirth,
		@PartTime, @Salaried, @AddressLine1, @AddressLine2,
		@City, @PostalCode, @LocalNumber, @OtherNumber, 
		@Email, @EmergencyName, @EmergencyNumber, @ContactRelation,
		@Gender, @HireDate, @OriginalHireDate, @SalaryStartDate, 
		@HourlySalary, @AnnualSalary, @ClassSequence,
		@BoClassSequence, @LastUpdatedDate, @RevenueCenterSeq, 
		@TerminationDate, @TerminationNote, @Status, @UpdateRecord,
		@PayrollId, @PasswordId
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @UpdateRecord = 1
		BEGIN
				SET @IsTerminated = 0
				
				IF @TerminationDate IS NULL 
				BEGIN
					SET @IsTerminated = 1
				END   
			
				--PRINT '				Updating ' + @employeeID + ' ' + @firstName +  ' ' + @lastName + ' ...' + CHAR(13)
				
				select @MyStr = 'ALTER PROCEDURE [dbo].[spDynamicSqlEmployeeUpdate]' + Char(13)
				select @MyStr = @MyStr + 'AS '  + Char(13)
				select @MyStr = @MyStr + 'BEGIN'  + Char(13)
				select @MyStr3 = ' END'

				select @MyStr2 = '	UPDATE OPENQUERY(' + @LinkedServer + ',''SELECT' + Char(13)
				select @MyStr2 = @MyStr2 + 'E.last_name, E.first_name, E.long_last_name, E.long_first_name, E.middle_name,' + Char(13)
				select @MyStr2 = @MyStr2 + 'E.chk_name, E.chk_name2, E.effective_from, E.date_of_birth, E.ob_part_time,' + Char(13)
				select @MyStr2 = @MyStr2 + 'E.ob_salaried, E.addr_ln_1, E.addr_ln_2, E.city, E.postal_code,' + Char(13)
				select @MyStr2 = @MyStr2 + 'E.local_num_1, E.other_local_num, E.e_mail_addr, E.emergency_name,' + Char(13)
				select @MyStr2 = @MyStr2 + 'E.emergency_number, E.contact_relation, E.gender,' + Char(13)
				select @MyStr2 = @MyStr2 + 'E.hire_date, E.original_hire_date, E.salary_start_date,' + Char(13)
				select @MyStr2 = @MyStr2 + 'E.hourly_salary, E.annual_salary, E.payroll_id, E.termination_date, E.emp_bo_class_seq,' + Char(13)
				
				IF @PasswordId <> '' OR @IsTerminated = 1
				BEGIN
					select @MyStr2 = @MyStr2 + 'E.termination_note, E.last_updated_date, E.emp_class_seq, E.id' + Char(13)			
				END
				ELSE
				BEGIN
					select @MyStr2 = @MyStr2 + 'E.termination_note, E.last_updated_date, E.emp_class_seq' + Char(13)			
				END
				
				select @MyStr2 = @MyStr2 + 'FROM micros.emp_def E WHERE E.obj_num = ' + @EmployeeID + ''')' + Char(13)
				select @MyStr2 = @MyStr2 + 'SET last_name = ''' +  LEFT(@LastName,16) + ''',' + Char(13)
				
				IF @BoClassSequence <> '0'
				BEGIN								
					select @MyStr2 = @MyStr2 + 'emp_bo_class_seq = ' + @BoClassSequence + ',' + Char(13)			
				END
				
				select @MyStr2 = @MyStr2 + 'first_name = ''' +  LEFT(@FirstName,8) + ''',' + Char(13)
				select @MyStr2 = @MyStr2 + 'long_last_name = ''' +  @LastName+ ''',' + Char(13)
				select @MyStr2 = @MyStr2 + 'long_first_name = ''' +  @FirstName+ ''',' + Char(13)
				select @MyStr2 = @MyStr2 + 'middle_name = ''' +  @MiddleName+ ''',' + Char(13)
				select @MyStr2 = @MyStr2 + 'chk_name = ''' +  LEFT(@FirstName + ' ' + LEFT(@LastName,1),8)+ ''',' + Char(13)
				select @MyStr2 = @MyStr2 + 'chk_name2 = ''' +  LEFT(@FirstName + ' ' + LEFT(@LastName,1),16)+ ''',' + Char(13)			
				
				IF @HireDate <> ''
				BEGIN
					select @MyStr2 = @MyStr2 + 'effective_from = ''' +  @EffectiveFrom+ ''',' + Char(13)
					select @MyStr2 = @MyStr2 + 'hire_date = ''' +  @HireDate+ ''',' + Char(13)
					select @MyStr2 = @MyStr2 + 'salary_start_date = ''' +  @SalaryStartDate+ ''',' + Char(13)
					select @MyStr2 = @MyStr2 + 'original_hire_date = ''' +  @OriginalHireDate+ ''',' + Char(13)
				END
				
				IF @DateOfBirth <> ''
				BEGIN 			
					select @MyStr2 = @MyStr2 + 'date_of_birth = ''' +  @DateOfBirth+ ''',' + Char(13)
				END
				
				select @MyStr2 = @MyStr2 + 'ob_part_time = ''' +  CASE WHEN @PartTime = '1' THEN 'T' ELSE 'F' END + ''',' + Char(13)
				select @MyStr2 = @MyStr2 + 'ob_salaried = ''' +  CASE WHEN @Salaried = '1' THEN 'T' ELSE 'F' END + ''',' + Char(13)
				select @MyStr2 = @MyStr2 + 'addr_ln_1 = ''' +  @AddressLine1+ ''',' + Char(13)
				select @MyStr2 = @MyStr2 + 'addr_ln_2 = ''' +  @AddressLine2+ ''',' + Char(13)
				select @MyStr2 = @MyStr2 + 'city = ''' +  @City+ ''',' + Char(13)
				select @MyStr2 = @MyStr2 + 'postal_code = ''' +  @PostalCode+ ''',' + Char(13)
				select @MyStr2 = @MyStr2 + 'local_num_1 = ''' +  @LocalNumber+ ''',' + Char(13)
				select @MyStr2 = @MyStr2 + 'other_local_num = ''' +  @OtherNumber+ ''',' + Char(13)
				select @MyStr2 = @MyStr2 + 'e_mail_addr = ''' +  LEFT(@Email,32) + ''',' + Char(13)
				select @MyStr2 = @MyStr2 + 'emergency_name = ''' +  @EmergencyName+ ''',' + Char(13)
				select @MyStr2 = @MyStr2 + 'emergency_number = ''' +  @EmergencyNumber+ ''',' + Char(13)
				select @MyStr2 = @MyStr2 + 'contact_relation = ''' +  @ContactRelation+ ''',' + Char(13)
				select @MyStr2 = @MyStr2 + 'gender = ''' +  CASE WHEN LTRIM(RTRIM(LOWER(@Gender))) = 'male' THEN 'M' ELSE 'F' END + ''',' + Char(13)
				select @MyStr2 = @MyStr2 + 'hourly_salary = ' + @HourlySalary + ',' + Char(13)
				select @MyStr2 = @MyStr2 + 'annual_salary = ' + @AnnualSalary + ',' + Char(13)
				select @MyStr2 = @MyStr2 + 'payroll_id = ' + @PayrollId + ',' + Char(13)
				
				IF @IsTerminated = 1
				BEGIN 
					select @MyStr2 = @MyStr2 + 'emp_class_seq = NULL,' + Char(13)
					select @MyStr2 = @MyStr2 + 'id = NULL,' + Char(13)				
				END
				ELSE
				BEGIN
					IF @ClassSequence <> '0'
					BEGIN
						select @MyStr2 = @MyStr2 + 'emp_class_seq = ' + @ClassSequence + ',' + Char(13)
					END
					
					IF @PasswordId <> ''
					BEGIN
						select @MyStr2 = @MyStr2 + 'id = ''' + @PasswordId + ''',' + Char(13)			
					END							
					
				END
				
				IF @TerminationDate <> ''
				BEGIN 
					select @MyStr2 = @MyStr2 + 'termination_date = ''' +  @TerminationDate + ''',' + Char(13)				
				END
				ELSE
				BEGIN
					select @MyStr2 = @MyStr2 + 'termination_date = NULL,' + Char(13)
				END
				
				select @MyStr2 = @MyStr2 + 'termination_note = ''' +  @TerminationNote+ ''',' + Char(13)
				select @MyStr2 = @MyStr2 + 'last_updated_date = ''' +  convert(varchar, getdate(), 126) + '''' + Char(13)
				select @MyStrAll = @MyStr+@MyStr2+@MyStr3
				--print @MyStrAll
				
				BEGIN TRY
					exec (@MyStrAll);
					Exec spDynamicSqlEmployeeUpdate
				END TRY
				BEGIN CATCH	

					DECLARE @Error VARCHAR(MAX)
										
					SET @Error =  CAST(ERROR_NUMBER() as varchar(32)) 
						+ ' ' + CAST(ERROR_SEVERITY() as varchar(32))
						+ ' ' + CAST(ERROR_STATE() as varchar(32))
						+ ' ' + CAST(ERROR_PROCEDURE() as varchar(256))
						+ ' ' + CAST(ERROR_LINE() as varchar(32))
						+ ' ' + CAST(ERROR_MESSAGE() as varchar(4000))

					PRINT '		spUpdateEmployees failed:' + @Error + CHAR(13)
					
					INSERT INTO [dbo].[Logs] ([Date], [Output], [StoredProcedure], [Location], DynamicSql)
					SELECT GETDATE(), @Error, '[dbo].[spUpdateEmployees]', @LocationName, @MyStrAll					
	
				END CATCH								 		
		
		END
			
		FETCH NEXT FROM CurEmployeeUpdate
		INTO
			@LocationId, @EmployeeId, @FirstName, @LastName, 
			@ObjectNumber, @LongFirstName, @LongLastName, @MiddleName,
			@CheckName, @CheckName2, @EffectiveFrom, @DateOfBirth,
			@PartTime, @Salaried, @AddressLine1, @AddressLine2,
			@City, @PostalCode, @LocalNumber, @OtherNumber, 
			@Email, @EmergencyName, @EmergencyNumber, @ContactRelation,
			@Gender, @HireDate, @OriginalHireDate, @SalaryStartDate, 
			@HourlySalary, @AnnualSalary, @ClassSequence,
			@BoClassSequence, @LastUpdatedDate, @RevenueCenterSeq, 
			@TerminationDate, @TerminationNote, @Status, @UpdateRecord,
			@PayrollId, @PasswordId
	END


	CLOSE CurEmployeeUpdate
	DEALLOCATE CurEmployeeUpdate

	


END


