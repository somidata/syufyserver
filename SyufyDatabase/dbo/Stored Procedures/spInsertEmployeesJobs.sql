﻿


-- =============================================
-- Author:		SomiData
-- Create date: 4/8/2016
-- Description:	Inserts Micros Job data into Micros POS.
-- =============================================
CREATE PROCEDURE [dbo].[spInsertEmployeesJobs]
	@LocationName varchar(100) = NULL, @LocationNumber varchar(50) = NULL, 
	@LinkedServer  varchar(100) = NULL, @IncrementalUpdate int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @MyStr varchar(Max), @MyStr2 varchar(Max), @MyStr3 varchar(Max), @MyStrAll varchar(Max),
			@EmployeeId varchar(100), @UpdateRecord varchar(100), @Salaried varchar(100), @JobId varchar(100),
			@EffectiveDate varchar(100), @CurrentDate date, @Name varchar(100), @OverrideRegPayRate varchar(100), 
			@RateEffectiveDateTime varchar(100), @PrimaryJob varchar(100), @ReturnValue int,
			@JobExists varchar(100), @EmployeeSequence varchar(100), @EffectiveFrom varchar(100), @JobSequence varchar(100)
						
	DECLARE CurEmployeeJobInsert CURSOR FOR
	SELECT 
		E.EmployeeId, E.EmployeeSequence , E.UpdateRecord, E.Salaried, E.EffectiveFrom,
		J.JobId, J.Name , J.OverrideRegPayRate, J.RateEffectiveDateTime, J.PrimaryJob, J.JobSequence
	FROM [dbo].[EmployeesLive] E 
	INNER JOIN [dbo].[JobsLive] J ON E.EmployeeId = J.EmployeeId
	WHERE E.LocationId = @LocationNumber

	OPEN CurEmployeeJobInsert
	FETCH NEXT FROM CurEmployeeJobInsert
	INTO
		@EmployeeId, @EmployeeSequence , @UpdateRecord, @Salaried, @EffectiveFrom,
		@JobId, @Name , @OverrideRegPayRate, @RateEffectiveDateTime, @PrimaryJob, @JobSequence
	WHILE @@FETCH_STATUS = 0
	BEGIN 
			
		--PRINT '@EmployeeId: ' + @EmployeeId + CHAR(13)
		--PRINT '@UpdateRecord: ' + @UpdateRecord + CHAR(13)
		--PRINT '@JobSequence: ' + @JobSequence + CHAR(13)

		IF @UpdateRecord = 1 AND @JobSequence <> 'NULL'
		BEGIN
		
			SET @CurrentDate = CAST(GETDATE() AS DATE)

			--SELECT @CurrentDate, @EffectiveDate
			--PRINT '@CurrentDate: ' + CAST(@CurrentDate AS VARCHAR(100)) + ' @EffectiveFrom: ' + CAST(@EffectiveFrom AS VARCHAR(100)) + CHAR(13)

			IF @CurrentDate >= @EffectiveFrom
			BEGIN
				
				select @MyStr = 'ALTER PROCEDURE [dbo].[spDynamicSqlEmployeeJobExists]' + Char(13)
				select @MyStr = @MyStr + ' @JobExists int output '  + Char(13)
				select @MyStr = @MyStr + ' AS '  + Char(13)
				select @MyStr = @MyStr + ' BEGIN'  + Char(13)
				select @MyStr3 = ' END'

				

				select @MyStr2 = ' SET @JobExists = (CASE ' + CHAR(13)
				select @MyStr2 = @MyStr2 + '	WHEN EXISTS (SELECT * FROM OPENQUERY(' + @LinkedServer +  ', ''SELECT TOP(1) * FROM micros.job_rate_def P WHERE job_seq =' + @JobSequence + ' AND emp_seq =' + @EmployeeSequence + ''')) THEN 1' + Char(13)
				select @MyStr2 = @MyStr2 + '    ELSE 0 END) ' + CHAR(13)
															
				select @MyStrAll = @MyStr+@MyStr2+@MyStr3			
				
				--print @MyStrAll + CHAR(13)

				BEGIN TRY				
					exec (@MyStrAll)

					EXEC @returnValue = spDynamicSqlEmployeeJobExists @JobExists = @JobExists OUTPUT	
				END TRY
				BEGIN CATCH					
					DECLARE @Error1 VARCHAR(MAX)
											
					SET @Error1 =  CAST(ERROR_NUMBER() as varchar(32)) 
						+ ' ' + CAST(ERROR_SEVERITY() as varchar(32))
						+ ' ' + CAST(ERROR_STATE() as varchar(32))
						+ ' ' + CAST(ERROR_PROCEDURE() as varchar(256))
						+ ' ' + CAST(ERROR_LINE() as varchar(32))
						+ ' ' + CAST(ERROR_MESSAGE() as varchar(4000))
					
					PRINT '		spDynamicSqlEmployeeJobExists failed:' + @Error1 + CHAR(13)												

					INSERT INTO [dbo].[Logs] ([Date], [Output], [StoredProcedure], Location, DynamicSql)
					SELECT GETDATE(), @Error1, '[dbo].[spInsertEmployees]', @LocationName, @MyStrAll
						
	
				END CATCH			
				
				IF @JobExists = 0
				BEGIN
				
					select @MyStr = 'ALTER PROCEDURE [dbo].[spDynamicSqlEmployeeJobInsert]' + Char(13)
					select @MyStr = @MyStr + 'AS '  + Char(13)
					select @MyStr = @MyStr + 'BEGIN'  + Char(13)
					select @MyStr3 = ' END'
												
					select @MyStr2 = '	INSERT OPENQUERY(' + @LinkedServer + ',''SELECT ' 
					select @MyStr2 = @MyStr2 + 'E.emp_seq, E.job_seq, E.override_reg_pay_rate, E.rate_effective_datetime, E.last_updated_date, E.ob_primary_job' + Char(13)
					select @MyStr2 = @MyStr2 + 'FROM micros.job_rate_def E '')' + Char(13)
					
					select @MyStr2 = @MyStr2 + 'VALUES (''' +  @EmployeeSequence + ''', ''' +  @JobSequence + ''', ''' + @OverrideRegPayRate + ''', '''  
					select @MyStr2 = @MyStr2 + @RateEffectiveDateTime + ''', ''' + convert(varchar, getdate(), 126) + ''', '''+ CASE WHEN @PrimaryJob = '1' THEN 'T' ELSE 'F' END + ''')' + Char(13)													
					select @MyStrAll = @MyStr+@MyStr2+@MyStr3
					--print @MyStrAll
					
					BEGIN TRY					
						exec (@MyStrAll);
						Exec spDynamicSqlEmployeeJobInsert
					END TRY
					BEGIN CATCH					
						
					DECLARE @Error VARCHAR(MAX)
										
						SET @Error =  CAST(ERROR_NUMBER() as varchar(32)) 
							+ ' ' + CAST(ERROR_SEVERITY() as varchar(32))
							+ ' ' + CAST(ERROR_STATE() as varchar(32))
							+ ' ' + CAST(ERROR_PROCEDURE() as varchar(256))
							+ ' ' + CAST(ERROR_LINE() as varchar(32))
							+ ' ' + CAST(ERROR_MESSAGE() as varchar(4000))
						
						PRINT '		spInsertEmployeesJobs failed:' + @Error + CHAR(13)


						INSERT INTO [dbo].[Logs] ([Date], [Output], [StoredProcedure], [Location], [DynamicSql])
						SELECT GETDATE(), @Error, '[dbo].[spInsertEmployeesJobs]', @LocationName, @MyStrAll
						

					END CATCH						
					
				END
			END		
			--ELSE
			--BEGIN
			--	PRINT 'NOT TRUE' + CHAR(13)
			--END
		END
			
		FETCH NEXT FROM CurEmployeeJobInsert
		INTO 
			@EmployeeId, @EmployeeSequence , @UpdateRecord, @Salaried, @EffectiveFrom,
			@JobId, @Name , @OverrideRegPayRate, @RateEffectiveDateTime, @PrimaryJob, @JobSequence
	END


	CLOSE CurEmployeeJobInsert
	DEALLOCATE CurEmployeeJobInsert

	


END












