﻿using System;
using System.Data.SqlClient;

namespace SyufyExecuteSql
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                using (SqlConnection con = new SqlConnection("Data Source=IT-SQL03;Database=HRIS_INT;Trusted_Connection=True;Pooling=false;"))
                {
                    con.Open();
                    
                    using (SqlCommand command = new SqlCommand("EXEC spMain", con))
                    {
                        command.ExecuteNonQuery();
                    }
                }

                Console.WriteLine(String.Format("SUCCESS"));
            }
            catch (SqlException e)
            {
                Log(e.ToString());
            }
            catch (Exception e)
            {
                Log(e.ToString());
            }
        }

        static void Log(string message)
        {
            Console.WriteLine("");
            Console.WriteLine(String.Format("EXCEPTION"));
            Console.WriteLine(String.Format("{0}", message));
            Console.WriteLine("");
        }
    }
}
