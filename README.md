# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary

	This is a RESTful Web API application built on the .NET Framework.  The application receives HTTP requests
	with employee/job content and proceeds to load into a MSSQLS database.  It then executes stored 
	procedures that load this data into a Micros POS server using linked server technology.    

* Version

	1.0.0.0

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

	Create the database objects in the SyufyDatabase project on a MSSQLS.  Keep in mind that the stored procedures
	call a linked server which connects to a POS that also must be configured.  Create a website on 
	an IIS server and publish the contents of the SyufyServer project.  Add an SSL certificate to the server and 
	bind the website to a https.  In that directory also copy the SyufyExecuteSql.exe from 
	the SyufyExecuteSql project.  Test the application by running the SyufyClient.exe in the 
	SyufyClient project.  This .exe registers a user, requests authorization token, and submits sample employee/job
	data.

* Configuration

	A record in the dbo.SyufyLocations must be inserted with a location id and the name of the installed linked 
	server.  A record must be created in the dbo.SyufyUsers with an email.  The user must execute the Register
	web service in order to be registered.  We limit the users who can register to the records in the dbo.SyufyUsers
	table.  Make sure the config files in the SyufyClient/SyufyServer/SyufyExecuteSql projects point to the 
	appropriate database.  

* Dependencies

	An SSL certificate to encrypt requests/responses on the IIS server.  The SyufyExecuteSql .exe must be in the 
	site directory.  This project executs a stored procedure which is a workaround to Entity Framework elevating the 
	execution of stored procedures to a distributed transaction.  By executing .exe the stored procedure is 
	not elevated and thus not a distributed transaction.  The linked server which connects to the POS. 
	Please see the SomiDataSoftwareWebServices.pdf document which provides detail information regarding 
	the Web API component.

* Database configuration

	There must be a location id record in the dbo.SyufyLocations and a record in the dbo.DefaultEmployeeTemplate.
	The record in the dbo.SyufyLocations table must have a name that matches an existing linked server 
	in the LinkedServer field.  Every new location added must have a record here and a valid linked sever name.
	This means for every new location you must manually add a linked server.  There must be a record in the
	dbo.DefaultEmployeeTemplate for every location.  This table sets the default security, manager level, and
	revenue center for employees.  These fields must have a value that matches the name in the Micros POS 
	database.  The table currently has one row for location id 1.  When another location is added you can simply
	copy the contents of that row and re-insert with the new location id and be ok assuming the new Micros POS
	names match.  Please see the SomiDataSoftwareDatabaseObjects file withing the solution  This file contains
	detailed description of the database tables and stored procedures.


* How to run tests

	We ran tests by running the SyufyClient.exe in the SyufyClient project.  This has code to register a user,
	retrieve token, and insert two employee records.  You might have to change the web.config which has the 
	base uri for the endpoint.

* Deployment instructions

	Create an IIS site on IIS Server and publish the contents of SyufyServer project into said website direcotry.
	Add the SyufyExecuteSql.exe from the SyufyExecuteSql project in the website directory as well.  Verify that
	web.config points to the appropriate database.

### Contribution guidelines ###

* Writing tests

	Tests were not documented but extensive testing was performed ranging from unit to system testing.  As well 
	as performance testing.

* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin

	Jose Reynoso (jreyn003)

* Other community or team contact

	customersupport@somidata.com