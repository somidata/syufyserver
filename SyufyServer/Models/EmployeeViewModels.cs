﻿namespace SyufyServer.Models
{
    // Models returned by EmployeeController actions.

    public class EmployeeViewModel
    {
        public string EmployeeId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
    
}
