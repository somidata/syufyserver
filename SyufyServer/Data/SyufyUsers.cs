﻿using System;

namespace SyufyServer.Data
{
    public class SyufyUsers
    {
        public int Id { get; set; }

        public string Email { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}